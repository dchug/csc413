import java.applet.Applet;
import java.lang.*;
import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Panel;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.*;

import javax.swing.JFrame;


public class Calci extends Applet implements ActionListener {
	
	Evaluator anEvaluator = new Evaluator();
	String expr;
	Button b[]=new Button[14];
	Button add,sub,mul,div,clear,C,EQ,openPara,closePara;
	Panel p = new Panel();
	TextField t1 = new TextField();
	
	char Pstr = ' ';
		public void init()
		{
			
			setLayout(new BorderLayout());
			add(t1, BorderLayout.NORTH);
			add(p, BorderLayout.CENTER);
			//t1=new TextField(10);
			GridLayout gl=new GridLayout(5,4);
			p.setLayout(gl);
			
			setBackground(Color.RED);
			//String expr = "1+30-8/7+54";
			//System.out.println(expr+ "= " +anEvaluator.eval(expr));
			if(Pstr == '=')
			{
				t1.setText(" ");
				Pstr = ' ';
			}
			for(int i=0;i<10;i++)
			{
				
				b[i]=new Button(""+i);
			}
		Button add = new Button("+");
		Button sub = new Button("-");
		Button mul = new Button("*");
		Button div = new Button("/");
		Button EQ = new Button("=");
		Button clear = new Button("Clear");
		Button C = new Button("C");
		Button openPara = new Button ("(");
		Button closePara = new Button (")");
		
		
			for(int i=7;i<10;i++)
			{
				p.add(b[i]);
			}
			 p.add(div);
			
			for(int i=4;i<7;i++)
			{
				p.add(b[i]);
			}
			p.add(mul);
			for(int i=1;i<4;i++)
			{
				p.add(b[i]);
			}
			p.add(sub);
			p.add(b[0]);
	        p.add(add);	
	        p.add(EQ);
	        p.add(clear);
	        p.add(C);
	        p.add(openPara);
	        p.add(closePara);
	     
	        
			
			for(int i=0;i<10;i++)
			{
				b[i].addActionListener(this);
			}
			add.addActionListener(this);
			sub.addActionListener(this);
			mul.addActionListener(this);
			div.addActionListener(this);
			EQ.addActionListener(this);
			clear.addActionListener(this);
			C.addActionListener(this);
			openPara.addActionListener(this);
			closePara.addActionListener(this);
		}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub
		
		if(Pstr == '=')
		{
			t1.setText(" ");
			Pstr = ' ';
		}
		String str=arg0.getActionCommand();
		char ch=str.charAt(0);
		
		
		Boolean eq = str.equals("=");
		Boolean clear = str.equals("Clear");
		Boolean C = str.equals("C");
		
		if(!(eq||clear||C))
		{
		t1.setText(t1.getText()+str);
		}
		
		else if(str.equals("="))
		{
		expr=t1.getText();
		t1.setText(expr+"= "+anEvaluator.eval(expr)); 
		Pstr = ch;
		}
		
		else if(str.equals("Clear"))
		{
			t1.setText(" ");
		}
		else if(str.equals("C"))
		{
			t1.setText(t1.getText().substring(0,t1.getText().length()-1));
		}
		
		 
			
	}
	public static void main(String args[])
	{	
		Calci f1 = new Calci();
		JFrame frame = new JFrame();
		f1.setPreferredSize(new Dimension(100,200));
		frame.getContentPane().setBackground(Color.BLUE );
		 frame.add(f1);
		 frame.pack();
		 frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		 frame.setResizable(false);
		 frame.setLocationRelativeTo(null);
		 frame.setVisible(true);
		f1.init();
		
		
	}
}