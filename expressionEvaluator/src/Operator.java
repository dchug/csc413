
import java.util.HashMap;


public abstract  class Operator {
	
	static String tok;static Object o;
	static HashMap<Object,Object> operators = new HashMap();
	public Operator(String tok2) {
		// TODO Auto-generated constructor stub
		this.tok = tok;
	}

	@SuppressWarnings("deprecation")
	//HashMap Operators = new HashMap();
	public abstract String execute(Operand op1,Operand op2);
	public static void hash()
	{
		operators.put("+",new addition(tok));
		operators.put("!",new finish(tok));
		operators.put("-",new substract(tok));
		operators.put("*",new multiply(tok));
		operators.put("/",new division(tok));
		operators.put("#",new division(tok));
		operators.put("(", new openPara(tok));
		operators.put(")", new closePara(tok));
		
	}
	/*public int priority() {
		   if(tok == "+" || tok == "-")
		   {
			   return 2;
		   }
		   else if (tok == "*" || tok == "/")
		   {
			   return 3;
		   }
		   else if (tok == "!")
		   {
			   return 1;
		   }
		   else if (tok == "#")
		   {
			   return 0;
		   }
		   else
		return 0;
	} */
	public abstract int inpriority();
	public abstract int outpriority();
	public static boolean check(String tok) {
		// TODO Auto-generated method stub
		
		o = operators.get(tok);
		//System.out.println("Checking Operator");
		if(o != null)
			return true;
		
		return false;
		
	}
	public static Object getOperation()
	{
		return o;
	}
	
}

class opinit extends Operator
{

	public opinit(String tok2) {
		super(tok2);
		// TODO Auto-generated constructor stub
	}

	@Override
	public String execute(Operand op1, Operand op2) {
		// TODO Auto-generated method stub
		return null;
	}
	public int inpriority()
	{
		return 1;
	}
	public int outpriority()
	{
		return 1;
	}
	
	
}
class finish extends Operator
{

	public finish(String tok2) {
		super(tok2);
		// TODO Auto-generated constructor stub
	}

	@Override
	public String execute(Operand op1, Operand op2) {
		// TODO Auto-generated method stub
		return null;
	}
	public int inpriority()
	{
		return 0;
	}
	public int outpriority()
	{
		return 0;
	}
	
}

class addition extends Operator
{
    
   public addition (String tok)
   {
	   super(tok);
   }
	@Override
  public String execute(Operand op1,Operand op2) {
		// TODO Auto-generated method stub
		int temp ;
		int p1 = op1.value();
		int p2 = op2.value();
		//int p2 = Integer.parseInt(op2);
		//System.out.println("op1 for addition "+op1.value());
		//System.out.println("op2 for addition "+op2.value());
		temp = p1+p2;
		//System.out.println("addition "+temp);
		String result = Integer.toString(temp);
		
		return result;
	}
	public int inpriority()
	{
		return 3;
	}
	public int outpriority()
	{
		return 3;
	}
}

class substract extends Operator
{

	public substract(String tok) {
		super(tok);
		// TODO Auto-generated constructor stub
	}


	@Override
	public String execute(Operand op1, Operand op2) {
		// TODO Auto-generated method stub
		int temp;
		temp = op1.value()- op2.value();
		String result = Integer.toString(temp);
		return result;
	}
	public int inpriority()
	{
		return 3;
	}
	public int outpriority()
	{
		return 3;
	}
	}

class multiply extends Operator
{

	public multiply(String tok2) {
		super(tok2);
		// TODO Auto-generated constructor stub
	}

	@Override
	public String execute(Operand op1, Operand op2) {
		// TODO Auto-generated method stub
		int temp;
		temp = op1.value()*op2.value();
		String result = Integer.toString(temp);
		return result;
	}
	public int inpriority()
	{
		return 4;
	}
	public int outpriority()
	{
		return 4;
	}
}

class division extends Operator
{

	public division(String tok2) {
		super(tok2);
		// TODO Auto-generated constructor stub
	}

	@Override
	public String execute(Operand op1,Operand op2) {
		// TODO Auto-generated method stub
		int temp;
		temp = op1.value()/ op2.value();
		String result = Integer.toString(temp);
		return result;
	}
	public int inpriority()
	{
		return 4;
	}
	public int outpriority()
	{
		return 4;
	}

}
class openPara extends Operator
{

	public openPara(String tok2) {
		super(tok2);
		// TODO Auto-generated constructor stub
	}

	@Override
	public String execute(Operand op1, Operand op2) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int inpriority() {
		// TODO Auto-generated method stub
		return 2;
	}
	public int outpriority() {
		// TODO Auto-generated method stub
		return 5;
	}
	
}
class closePara extends Operator
{

	public closePara(String tok2) {
		super(tok2);
		// TODO Auto-generated constructor stub
	}

	

	@Override
	public int inpriority() {
		// TODO Auto-generated method stub
		return 2;
	}
	public int outpriority() {
		// TODO Auto-generated method stub
		return 2;
	}

	@Override
	public String execute(Operand op1, Operand op2) {
		// TODO Auto-generated method stub
		return null;
	}
	
}

