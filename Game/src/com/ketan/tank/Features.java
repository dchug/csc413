package com.ketan.tank;

import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;

public class Features implements MouseListener{
	BufferedImage image;Graphics g;
	private double x,y;
	private int i=0;Tank tank;
	private int shoot=10,Damage=20,shot_speed=7,Armour_saving=5,max_speed=4,Degrees_of_turn=1;
public Features(double x,double y,BufferedImage image,Graphics g,Tank tank)
{
	this.x =x;
	this.y = y;
	this.image = image;
	this.g=g;
	this.tank = tank;
}
public void tick(){
	
}
public void render(Graphics g){
	g.drawImage(image, (int)x,(int) y, null);
	//g.drawRect((int)x+78, (int)y+10, 150, 20);
	//shots per second
	g.drawString("Shots Per Second",(int)x+80,(int)y+20);
	g.drawString(Integer.toString(shoot), (int)x+230, (int)y+20);
	//Damage rate
	//g.drawRect((int)x+78, (int)y+30, 150, 20);
	g.drawString("Shot Damage",(int)x+80,(int)y+40);
	g.drawString(Integer.toString(Damage),(int)x+230,(int)y+40);
	//Shot Speed
	//g.drawRect((int)x+78, (int)y+50, 150, 20);
	g.drawString("Shot Speed",(int)x+80,(int)y+60);
	g.drawString(Integer.toString(shot_speed),(int)x+230,(int)y+60);
	//Armour Saving
	//g.drawRect((int)x+78, (int)y+70, 150, 20);
	g.drawString("Armour Saving %",(int)x+80,(int)y+80);
	g.drawString(Integer.toString(Armour_saving),(int)x+230,(int)y+80);
	//Max speed
	//g.drawRect((int)x+78, (int)y+90, 150, 20);
	g.drawString("Max Speed",(int)x+80,(int)y+100);
	g.drawString(Integer.toString(max_speed),(int)x+230,(int)y+100);
	//Degrees of turn
	//g.drawRect((int)x+78, (int)y+110, 150, 20);
	g.drawString("Degrees of Turn",(int)x+80,(int)y+120);
	g.drawString(Integer.toString(Degrees_of_turn),(int)x+230,(int)y+120);
}
@Override
public void mouseClicked(MouseEvent arg0) {
	// TODO Auto-generated method stub
	
}
@Override
public void mouseEntered(MouseEvent arg0) {
	// TODO Auto-generated method stub
	
}
@Override
public void mouseExited(MouseEvent arg0) {
	// TODO Auto-generated method stub
	
}
@Override
public void mousePressed(MouseEvent arg0) {
	// TODO Auto-generated method stub
	i++;
	System.out.println(i);
}
@Override
public void mouseReleased(MouseEvent arg0) {
	// TODO Auto-generated method stub
	
}
public double getX()
{
	return x;
}
public double getY()
{
	return y;
}
public void setShoot(int i)
{
	this.shoot +=i;
	
	if(shoot<=1)
		shoot=1;
	
	if(shoot>=15)
		shoot=15;
	
}
public int getShoot()
{
	return shoot;
}
public void setDamage(int i) {
	// TODO Auto-generated method stub
	 this.Damage +=i;
	 
	if(Damage >=500)
		Damage =500;
	
	if(Damage <=5)
		Damage =5;
}
public int getDamage()
{
	return Damage;
}
public void shotSpeed(int i){
	this.shot_speed +=i;
	
	if(shot_speed >=30)
		shot_speed=30;
	
	if(shot_speed <=7)
	shot_speed =7;
}
public int getShotSpeed()
{
	return shot_speed;
}
public void setArmour(int i)
{
this.Armour_saving +=i;
	
	if(Armour_saving>=90)
		Armour_saving=90;
	
	if(Armour_saving <=5)
		Armour_saving =5;
	
}
public int getArmour(){
	return Armour_saving;
}
public void setMaxSpeed(int i)
{
this.max_speed +=i;
	
	if(max_speed>=16)
		max_speed=16;
	
	if(max_speed<=1)
		max_speed=1;
	
}
public int getMAxSpeed()
{
	return max_speed;
}
public void setDegreeOfTurn(int i)
{
this.Degrees_of_turn +=i;
	
	if(Degrees_of_turn>=20)
		Degrees_of_turn=20;
	
	if(Degrees_of_turn <=1)
		Degrees_of_turn =1;
	
}
public int getDegreeOfTurn()
{
	return Degrees_of_turn;
}
}
