package com.ketan.tank;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

public class Background {
	
	private BufferedImage background;
	private int imageWidth,widthCount;
	private int imageHeight,heightCount;
	private int frameWidth;
	private int frameHeight;
	public Background(BufferedImage image,int width,int height, int scale)
	{
		this.background = image;
		this.imageWidth = background.getWidth();
		this.imageHeight = background.getHeight();
		this.frameWidth = width*scale;
		this.frameHeight = height*scale;
		this.widthCount = frameWidth/imageWidth;
		this.heightCount = frameHeight/imageHeight;
	}

	public void render(Graphics g)
	{
		for(int i = 0; i<=widthCount;i++){
			for(int j = 0; j<=heightCount;j++){
			g.drawImage(background, i*300, j*240, null);
		}
	}
	}
}
