package com.ketan.tank;

import java.awt.Graphics;
import java.awt.image.BufferedImage;

public class smallExplosion {
     BufferedImage[] small = new BufferedImage[6];
     Animation an;private int count;
     private  double x;
	private  double y;
	public smallExplosion(double x, double y,Tank tank){
		this.x = x;
		this.y = y;
		this.small[0] = tank.smallExplosion1();
		this.small[1] = tank.smallExplosion2();
		this.small[2] = tank.smallExplosion3();
		this.small[3] = tank.smallExplosion4();
		this.small[4] = tank.smallExplosion5();
		this.small[5] = tank.smallExplosion6();
		 
		count =5;
		
		an = new Animation(3,small[0],small[1],small[2],small[3],small[4],small[5]);
		
	}
	public void tick (){
		an.runAnimation();
	
	}
	public void render(Graphics g){
		an.drawAnimation(g, (int) x,(int) y, 0);
		count--;
	}
	public void setX(double x){
		this.x=x;
	}
	public void setY(double y){
		this.y = y;
	}
	public int getCount(){
		return count;
	}
}
