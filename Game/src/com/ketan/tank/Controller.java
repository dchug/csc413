package com.ketan.tank;

import java.awt.Graphics;
import java.util.ArrayList;

import com.ketan.rainbowGame.Buttons;
import com.ketan.rainbowGame.life;
import com.ketan.rainbowGame.rainPlayer;

public class Controller {
	public ArrayList<Entity> e = new ArrayList<Entity>();
	public ArrayList<EntityA> ea = new ArrayList<EntityA>();
	public ArrayList<Entity> e2 = new ArrayList<Entity>();
	public ArrayList<smallExplosion> s = new ArrayList<smallExplosion>();
	public ArrayList<largeExplosion> l = new ArrayList<largeExplosion>();
	public ArrayList<Buttons> b = new ArrayList<Buttons>();
	public ArrayList<life> life = new ArrayList<life>();
	public ArrayList<rainPlayer> player = new ArrayList<rainPlayer>();
	
	Entity ent;Tank tank;Buttons bt;
	EntityA enta;smallExplosion SE;largeExplosion LE;
	life li;rainPlayer P;
	
	
	public void tick()
	{
		
		for (int i =0 ;i<e.size();i++)
		{
			ent = e.get(i);
			ent.tick();
			if(ent.getY() < 0)
				removeEntity(ent);
		}
		for (int i =0 ;i<e2.size();i++)
		{
			ent = e2.get(i);
			ent.tick();
			if(ent.getY() < 0)
				removeEntity2(ent);
		}
		for (int i =0 ;i<ea.size();i++)
		{
			enta = ea.get(i);
			enta.tick();
		}
		//small Explosion
		for(int i =0;i<s.size();i++){
			SE = s.get(i);
			SE.tick();
			  if(SE.getCount() < 0)
			  {
				  removeExplosion(SE);
			  }
		}
		//Large Explosion
		for(int i =0;i<l.size();i++){
			LE = l.get(i);
			LE.tick();
			  if(LE.getCount() < 0)
			  {
				  removeExplosion(LE);
			  }
		}
		for (int i=0;i<b.size();i++)
		{
			bt = b.get(i);
			bt.tick();
			
		}
		//for the life of rainbow game
	 for(int i=0;i<life.size();i++)
	 {
		 li = life.get(i);
		 li.tick();
	 }
	 //for rainBow Player
		
	 for(int i=0;i<player.size();i++)
	 {
		 P = player.get(i);
		 P.tick(this);
	 }
	}
	public void render(Graphics g)
	{
		for(int i=0;i<e.size();i++)
		{
			ent = e.get(i);
			ent.render(g);
			
		}
		for(int i=0;i<e2.size();i++)
		{
			ent = e2.get(i);
			ent.render(g);
		}
		for(int i=0;i<ea.size();i++)
		{
			enta = ea.get(i);
			enta.render(g);
		}
	  //	Small Explosion
		for(int i=0;i<s.size();i++)
		{
			SE = s.get(i);
			SE.render(g);
		}
		//large Explosion
		for(int i=0;i<l.size();i++)
		{
			LE = l.get(i);
			LE.render(g);
		}
		for(int i=0;i<b.size();i++)
		{
			bt =b.get(i);
			bt.render(g);
		}
		//for the life of rainbow game
		for (int i=0;i<life.size();i++)
		{
			li = life.get(i);
			li.render(g);
		}
		//for the rainbow player
		for(int i=0;i<player.size();i++)
		{
			P = player.get(i);
			P.render(g);
		}
		
	}
	//Bullet of first tank
	public void addEntity(Entity block)
	{
		e.add(block);
	}
	public void removeEntity(Entity block){
		e.remove(block);
	}
	//Bullet of second tank
	public void addEntity2(Entity block)
	{
		e2.add(block);
	}
	
	public void removeEntity2(Entity block){
		e2.remove(block);
	}
	//Entity A
	public void addEntityA(EntityA block)
	{
		ea.add(block);
	}
	public void removeEntityA(EntityA block){
		ea.remove(block);
	}
	//small Explosion
   public void addExplosion(smallExplosion block)
   {
	   s.add(block);
   }
   public void removeExplosion(smallExplosion block)
   {
	   s.remove(block);
   }
   //large Explosion
   public void addExplosion(largeExplosion block)
   {
	   l.add(block);
   }
   public void removeExplosion(largeExplosion block)
   {
	   l.remove(block);
   }
   //For score board
   public void addButton(Buttons block)
   {
	   b.add(block);
   }
   public void removeButton(Buttons block)
   {
	   b.remove(block);
   } 
   //adding the life to rainbow game
   public void addLife(life block)
   {
	   life.add(block);
   }
   public void removeLife(life block)
   {
	   life.remove(block);
   } 
   //adding the rainbow player
   public void addRainPlayer(rainPlayer block)
   {
	   player.add(block);
   }
   public void removeRainPlayer(rainPlayer block)
   {
	   player.remove(block);
   } 
 
}
