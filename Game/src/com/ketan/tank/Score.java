package com.ketan.tank;

import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;

public class Score {
	private Player p;private String score="0";Font f;
	Graphics g;private double pXView,pYView; 
 public Score(Player p,Graphics g){
	 this.p = p;
	 this.g =g;
	pXView = p.getX()-100; 
	pYView = p.getY()-100;
    
 }
public void tick(){
	pXView = p.getX()-100; 
	pYView = p.getY()-100;
	 
    if((pXView+390)>1280){
    	pXView =1280-290;
    }else if((pXView+450)>1600){
    	pXView=1600-450;
    }else if((pXView<0)){
    	pXView =0;
    }else if(pXView<0){
    	pXView =0;
    }
}
 
 public void render(Graphics g){
	 
Graphics2D g2 = (Graphics2D)g;
      g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
         RenderingHints.VALUE_ANTIALIAS_ON);
     g2.drawString("0", (int) 530,(int) 120); 
 }
 public void updateScore(String score)
 {
	 this.score= score;
 }
 public String getScore()
 {
	return score;
	 
 }
}
