package com.ketan.tank;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;

public class powerUp extends GameObject implements EntityA {
	private BufferedImage  power,PowerUp;
	private int i; SpriteSheet ss;
	private boolean p;
	
	public powerUp(BufferedImage image,double x,double y,int index,boolean pow){
		super(x,y);
		
		this.power = image;
		this.i = index;
		ss = new SpriteSheet(power);
		this.p = pow;
		PowerUp = ss.grabImage(i,1,64,64);
	}

	@Override
	public void tick() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void render(Graphics g) {
		// TODO Auto-generated method stub
		
		g.drawImage(PowerUp,(int) x,(int) y, null);
	}

	@Override
	public double getX() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public double getY() {
		// TODO Auto-generated method stub
		return 0;
	}
	public void setX(int x){
		this.x =x;
	}
	public void setY(int y)
	{
		this.y = y;
	}

	@Override
	public Rectangle getBound() {
		// TODO Auto-generated method stub
		 return new Rectangle((int) x,(int) y,64,64);
	}

	@Override
	public void setBroke(boolean broke) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public int disappearCount() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public boolean breakable() {
		// TODO Auto-generated method stub
		return false;
	}
    public boolean checkPowerUp(){
    	return p;
    }
    public int getindex(){
    	return i;
    }

	@Override
	public void setdisappearanceCount(int i) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public BufferedImage getWall() {
		// TODO Auto-generated method stub
		return PowerUp;
	}

	@Override
	public double getMaxX() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return null;
	}
}
