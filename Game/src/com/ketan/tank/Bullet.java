package com.ketan.tank;

import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;

public class Bullet extends GameObject implements Entity {
	private double velX; 
	private double velY;
	private BufferedImage bullet1,bullet2,bullet3,Bullet;private int i,speed,tankDraw;
	SpriteSheet ss1,ss2,ss3;Tank tank;
	public Bullet(Tank tank,double x,double y,int i,int tankDraw,int speed){
		super(x+12,y+12);
		this.tank = tank;
		this.i = i;
		this.speed = speed;
		this.velX = Math.cos(Math.toRadians(i*6));
		this.velY = Math.sin(Math.toRadians(i*6));
		this.bullet1 = tank.getBullet1();
		this.bullet2 = tank.getBullet2();
		this.bullet3 = tank.getBullet3();
		this.tankDraw = tankDraw;
		ss1 = new SpriteSheet(bullet1);
		ss2 = new SpriteSheet(bullet2);
		ss3 = new SpriteSheet(bullet3);
		if(tankDraw == 1)
		this.Bullet = ss1.grabBullet(i, 1, 24, 24);
		if(tankDraw == 2)
			this.Bullet = ss2.grabBullet(i, 1, 24, 24);
		if(tankDraw == 3)
			this.Bullet =ss3.grabBullet(i, 1, 24, 24);
		
	}
    public void tick()
    { 
    	
    	x +=(velX*speed);
    	y -= (velY*speed);
    }
    public void render(Graphics g)
    {
    	g.drawImage(Bullet,(int) x,(int) y, null);
    	
    }
    public double getX()
    {
    	return x;
    }
    public double getY()
    {
    	return y;
    }
    public Rectangle getBound()
    {
   	 return new Rectangle((int) x,(int) y,24,24);
    }
	public BufferedImage getBullet(){
		return Bullet;
	}
	@Override
	public void setVelX(int i) {
		// TODO Auto-generated method stub
		
	}
    
}
