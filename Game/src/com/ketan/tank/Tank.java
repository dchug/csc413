package com.ketan.tank;



import java.applet.AudioClip;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class Tank extends JPanel implements Runnable,MouseListener {


	private static final long serialVersionUID = 1L;
   public static  final int WIDTH =320;
   public static  final int HEIGHT =400;//WIDTH/12*9;
   public static  final int SCALE =4;
   public final String Title = "Tank War";
  
   
   Graphics g ;
   private boolean running = false,reset1=false,reset2=false;
   private boolean is_shooting = false,is_shooting2 = false,p1right = false,p1left = false,
		   p2right = false,p2left = true;
   boolean coll = false;
   private Thread thread;
   private AudioClip small_explosion;
   
   private Random r = new Random();
   
   private BufferedImage image = new BufferedImage(WIDTH*SCALE,HEIGHT*SCALE,BufferedImage.TYPE_INT_RGB);
   private Image p1View,p2View;
   
   private BufferedImage Player,Player1,Player2,Player3,Bullet1,Bullet2,Bullet3,
   background,Wall1,Wall2,power,grey_basic,grey_heavy,grey_light;
   private BufferedImage[] small = new BufferedImage[7];
   private BufferedImage[] large = new BufferedImage[8];
   private int i =1,shoot_count1=0,shoot_count2 =0;
   private int i2 =1;
   private int tankDraw =0,resetCount1=0, resetCount2=0;
   
   //private ArrayList<Wall> W = new ArrayList<Wall>();
   
   
   private Player p;
   private Player p2;
   private Controller c;
   private Background back;
   private Collision collision;
   private smallExplosion SE;
   private powerUp pow;
   private Sound sound;private Score score;Font f;private Features F_light,F_basic,F_heavy;
   int count;Healthbar health,health2;private int p1Score=0,p2Score=0;private Features feat,feat2;
   
   
   public void init()   
   {
           
	   BufferedImageLoader loader = new BufferedImageLoader();
	   Player = loader.loadImage("/Tank_blue_base_strip60.png");
	   Player1 = loader.loadImage("/Tank_blue_light_strip60.png");
	   Player2 = loader.loadImage("/Tank_blue_basic_strip60.png");
	   Player3 = loader.loadImage("/Tank_blue_heavy_strip60.png");
	   Bullet1 = loader.loadImage("/Shell_light_strip60.png");
	   Bullet2 = loader.loadImage("/Shell_basic_strip60.png");
	   Bullet3 = loader.loadImage("/Shell_heavy_strip60.png");
	   background = loader.loadImage("/Background.png");
	   power = loader.loadImage("/Weapon_strip4.png");
	   Wall1 = loader.loadImage("/Blue_wall1.png");
	   Wall2 = loader.loadImage("/Blue_wall2.png");
	   grey_basic = loader.loadImage("/Tank_grey_basic.png");
	   grey_heavy = loader.loadImage("/Tank_grey_heavy.png");
	   grey_light = loader.loadImage("/Tank_grey_light.png");
	   
           
	   for(int i =1;i<7;i++){
	  small[i] = loader.loadImage("/explosion1_"+i+".png");
	   }
	   for (int i=1;i<8;i++){
		   large[i] = loader.loadImage("/explosion2_"+i+".png");
	   }
	   
	  
	  // marchThroughImage(Player);
	
		
	
	   addMouseListener(this);
	   addKeyListener(new KeyInput(this));
	   this.requestFocus();
		   c = new Controller();
		   p = new Player(1070,1166,this,1);
		    p2 = new Player(170,150,this,2);
	              
                   Drawmap("drawMap.txt");
		
                  
	  // c.addEntityA(new powerUp(power,5*32,32*5,3,this,true));
	    collision = new Collision(this);
	    
	    health = new Healthbar(p);
	    health2 = new Healthbar(p2); 
            back = new Background(background,WIDTH,HEIGHT,SCALE);
            sound = new Sound();
            F_light = new Features(0,600,grey_light,g,this);
            F_basic = new Features(425,600,grey_basic,g,this);
            F_heavy = new Features(850,600,grey_heavy,g,this);
          
       		
            sound.background("/Music.wav");
	   // SE = new smallExplosion(100,100,this);
	    
           
	   
	    
	  
   }
  
   private void marchThroughImage(BufferedImage image) {
	    int w = image.getWidth();
	    int h = image.getHeight();
	    System.out.println("width, height: " + w + ", " + h);
	 
	    for (int i = 0; i < h; i++) {
	      for (int j = 0; j < w; j++) {
	        System.out.println("x,y: " + j + ", " + i);
	        int pixel = image.getRGB(j, i);
	        printPixelARGB(pixel);
	        System.out.println("");
	      }
	    }
	  }
   public void printPixelARGB(int pixel) {
	    int alpha = (pixel >> 24) & 0xff;
	    int red = (pixel >> 16) & 0xff;
	    int green = (pixel >> 8) & 0xff;
	    int blue = (pixel) & 0xff;
	    System.out.println("argb: " + alpha + ", " + red + ", " + green + ", " + blue);
	  }
   public static boolean isPixelCollide(double x1, double y1, BufferedImage image1,
           double x2, double y2, BufferedImage image2) {
// initialization
double width1 = x1 + image1.getWidth() -1,
height1 = y1 + image1.getHeight() -1,
width2 = x2 + image2.getWidth() -1,
height2 = y2 + image2.getHeight() -1;

int xstart = (int) Math.max(x1, x2),
ystart = (int) Math.max(y1, y2),
xend   = (int) Math.min(width1, width2),
yend   = (int) Math.min(height1, height2);

// intersection rect
int toty = Math.abs(yend - ystart);
int totx = Math.abs(xend - xstart);

for (int y=1;y < toty-1;y++){
int ny = Math.abs(ystart - (int) y1) + y;
int ny1 = Math.abs(ystart - (int) y2) + y;

for (int x=1;x < totx-1;x++) {
int nx = Math.abs(xstart - (int) x1) + x;
int nx1 = Math.abs(xstart - (int) x2) + x;
try {
if (((image1.getRGB(nx,ny) & 0xFF000000) != 0x00) &&
(((image2.getRGB(nx1,ny1)) & 0xFF000000) != 0x00)) {
// collide!!
	//System.out.println("Collision");
return true;
}
} catch (Exception e) {
System.out.println("s1 = "+nx+","+ny+"  -  s2 = "+nx1+","+ny1);

}
}
}

return false;
}

public void Drawmap(String map)   {
	   // reader = new BufferedReader(new FileReader(map));
//		BufferedReader br = null;
//		int pos = 0;
//		try {
//			String line;
//			File file = null;
//			//ClassLoader classLoader = getClass().getClassLoader();
//			
//		   //file = new File(classLoader.getResource(map).getFile());
//		   InputStream in = getClass().getResourceAsStream("/drawMap.csv"); 
//		    br = new BufferedReader(new InputStreamReader(in));
//                        //file = new File(map);
//			
		Scanner mapFile = null;
	 // File mapFile = new File(map);
                InputStream in = getClass().getResourceAsStream("/drawMap.csv"); 
		mapFile = new Scanner(new BufferedReader(new InputStreamReader(in)));
    
           
		 mapFile.useDelimiter(",|\\n|\\r");
	     String nextLine = null;
            
		   for(int i=0;i<50;i++)
		    {
//                  // nextLine.split(Title) = 
			   for(int j=0;j<40;j++){
               		
                String next = mapFile.next();
           
            //System.out.println(next);
           
           if (next.equals("")) {
               j--;
               continue;
           }
           int n = Integer.parseInt(next);
           
           if(n == 1)
            {
        	  // System.out.println(j*32+"for j="+j+"for i="+i+" "+i*32);
        	  c.addEntityA(new Wall(Wall1,j*32,32*i,false,false,null));
             }
           if(n == 2)
           {
       	  // System.out.println(j*32+"for j="+j+"for i="+i+" "+i*32);
       	     c.addEntityA(new Wall(Wall2,j*32,32*i,true,false,null));
            }
           if(n==3)
           {
        	  int index = r.nextInt(100)%5;
        	  if(index==0)
        		  index++;
        	   c.addEntityA(new powerUp(power,j*32,32*i,index,true));
           }
              } 
		  }
		   mapFile.close();
		   
	   }
   
   
   private synchronized void start(){
	   if(running)
		   return;
	   
	   running = true;
	   thread = new Thread(this);
	   thread.start(); 
	   
   }
   private synchronized void stop()
   {
	   if(!running)
		   return;
	   
	   running = false;
	  
		   try {
			thread.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		   System.exit(1);
	   
		   
	   
   }

	@Override
	public void run() {
            
			
			
                
                init();
            
		
			
		
		long LastTime = System.nanoTime();
		final double amountofTicks = 60.0;
		double ns = 1000000000/amountofTicks;
		double delta = 0;
		int updates = 0;
		int frames = 0;long timer = System.currentTimeMillis();
		
		while(running)
		{
			//this is the game loop
			
                         
			long now = System.nanoTime(); 
			delta += (now - LastTime )/ns;			
			LastTime = now; 
			if(delta >= 1)
			{
				tick();
				updates++;
				delta--;
			}
			
			repaint();
			//render(g);
			

			frames++;
			
			if(System.currentTimeMillis() - timer > 1000){
				timer += 1000;
				//System.out.println(updates + "Ticks ,Fps "+ frames);
				updates = 0;
				frames = 0;
			}
			
		}
		stop();
	}
	public Graphics2D createGraphics2D(int w, int h) {
		Graphics2D g2 = null;
		if (image == null || image.getWidth() != w || image.getHeight() != h) {
			//System.out.println("inside if condition");
			image = (BufferedImage) createImage(w, h);
			
		}
		
		//g2 = image.createGraphics();
		
		g2 = image.createGraphics();
		g2.setBackground(getBackground());
		g2.setRenderingHint(RenderingHints.KEY_RENDERING,
				RenderingHints.VALUE_RENDER_QUALITY);
		g2.clearRect(0, 0, w, h);
		
		return g2;
	}
	public static BufferedImage resize(BufferedImage image, int width, int height) {
	    BufferedImage bi = new BufferedImage(width, height, BufferedImage.TRANSLUCENT);
	    Graphics2D g2d = (Graphics2D) bi.createGraphics();
	    g2d.addRenderingHints(new RenderingHints(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY));
	    g2d.drawImage(image, 0, 0, width, height, null);
	    g2d.dispose();
	    return bi;
	}
	@Override
  public void paint( Graphics g){
		 
		
		  // p1 = image.getSubimage((int)p2.getX()-1,(int) p2.getY()-1, 150, 150);
	     
	     // g.drawImage(resize,600,500,300,300,this);
	     // g.drawImage(p1,0,0,150,150,this);
		//g.drawImage(resize, 0, 0, this);

		Dimension windowSize = getPreferredSize();	
		//System.out.println(windowSize.width);
        Graphics2D g2 = createGraphics2D(windowSize.width, windowSize.height);
       // g2.scale(.3, 1);
      //  System.out.println(windowSize.height);
        g.setColor(Color.GRAY);
        g.fillRect(630, 0,30 ,600);
        g.setColor(new Color(255,69,0));
        g.fillRect(0, 600,windowSize.width+20 ,600);
        render(g2);
        
        Font font = new Font("Serif", Font.BOLD, 14);
        g.setFont(font);
        g.setColor(Color.BLACK);
        F_light.render(g);
        F_basic.render(g);
        F_heavy.render(g);
       // g2.dispose();
        
        
       double p2Xview = p2.getX()-170;
       double p2Yview = p2.getY()-150;
       double p1Xview = p.getX()-170;
       double p1Yview = p.getY()-150;
       
       
        if(p2Xview<0){
        	p2Xview =0;
        }else if(p2Yview<0){
        	//System.out.println("inside p2Yview <0");
        	p2Yview =0;
        }
       else if((p2Xview+390)>1280){
        	p2Xview=1280-390;
       }
       else if((p2Yview+600)>1600){
           // System.out.println("inside p2Yview >1600");
            p2Yview =1600-600;
        }
        
        
        if((p1Xview+390)>1280){
        	p1Xview =1280-390;
        }
        
        if((p1Yview+600)>1600){
        	//System.out.println("inside p1Yview >1600");
            p1Yview=1600-600;
        }
        
        if((p1Xview<0)){
        	p1Xview =0;
        }
        
        if(p1Yview<0){
        	p1Yview =0;
        }
        
       
        	
       // score.render(g2);
        //This is at left side
        p2View = image.getSubimage((int) p2Xview,(int) p2Yview, 390, 600);
        g.drawImage(p2View,0,0,630,600,this);
        
       // System.out.println("player is at "+p.getY());
        //This is right side
        p1View = image.getSubimage((int) p1Xview,(int) p1Yview,390,600);
        g.drawImage(p1View,windowSize.width/2+20,0,630,600,this);
        
        BufferedImage resize1 = resize(image,200,200);//image.getScaledInstance(200, 200, 0);
        g.drawImage(resize1, 540,400, this);
        
        Graphics2D gr1 = (Graphics2D)g;
        gr1.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);
            Font font1 = new Font("Serif", Font.PLAIN, 96);
            gr1.setFont(font1);gr1.setColor(Color.RED);
            gr1.drawString(Integer.toString(p1Score), (int) 530,(int) 120); 
            
            Graphics2D gr2 = (Graphics2D)g;
            gr2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                    RenderingHints.VALUE_ANTIALIAS_ON);
                Font font2 = new Font("Serif", Font.PLAIN, 96);
                gr2.setFont(font2);gr2.setColor(Color.blue);
            gr2.drawString(Integer.toString(p2Score), (int) windowSize.width/2+60,(int) 120);
            
            
            
            
            
            
          }
	
	
	private long SystemcurrentTimeMillis() {
		// TODO Auto-generated method stub
		return 0;
	}
	private void tick()
	{
		collision.b1p2(p2,c,sound,health2,feat2);
		
		collision.b2p1(p,c,sound,health,feat);
	    p.collision(c,sound,health,this);	
	    p2.collision(c,sound,health2,this);
		health.tick(collision);
		health2.tick(collision);
		if(shoot_count1>0)
			shoot_count1--;
	    if(shoot_count2>0)
	    	shoot_count2--;
		//collision = new Collision();
		
		
		for(int i=0;i<c.e.size();i++){
			
			for(int j=0;j<c.ea.size();j++){
				                //checks if it's a wall or PowerUp
				if((c.e.get(i).getBound().intersects(c.ea.get(j).getBound()))){
		       
					if(!c.ea.get(j).checkPowerUp()){  
			if(!c.ea.get(j).breakable())
			{
				if(isPixelCollide(c.e.get(i).getX(),c.e.get(i).getY(),c.e.get(i).getBullet(),c.ea.get(j).getX(),c.ea.get(j)
			    		   .getY(),Wall2)){
					System.out.println("");
				c.addExplosion(new smallExplosion(c.ea.get(j).getX(),c.ea.get(j).getY(),this));
				c.removeEntity(c.e.get(i));
				c.ea.get(j).setBroke(true);
				sound.Explosion("/Explosion_small.wav");
				
				break;
				}
			}
			else if(c.ea.get(j).breakable() && (c.ea.get(j).disappearCount() == 0)){
				c.addExplosion(new smallExplosion(c.e.get(i).getX(),c.e.get(i).getY(),this));
				c.removeEntity(c.e.get(i));
				c.ea.get(j).setBroke(true); 
				sound.Explosion("/Explosion_small.wav");
				break;
			}
				}
				
				}
				
			}
			
		}
        for(int i=0;i<c.e2.size();i++){
			
			for(int j=0;j<c.ea.size();j++){
				if((c.e2.get(i).getBound().intersects(c.ea.get(j).getBound()))){
					if(!c.ea.get(j).checkPowerUp()){
			if(!c.ea.get(j).breakable())
			{
				c.addExplosion(new smallExplosion(c.e2.get(i).getX(),c.e2.get(i).getY(),this));
				c.removeEntity2(c.e2.get(i));
				c.ea.get(j).setBroke(true);
				sound.Explosion("/Explosion_small.wav");
				break;
			}
			else if(c.ea.get(j).breakable() && (c.ea.get(j).disappearCount() == 0)){
				c.addExplosion(new smallExplosion(c.e2.get(i).getX(),c.e2.get(i).getY(),this));
				c.removeEntity2(c.e2.get(i));
				c.ea.get(j).setBroke(true);
				sound.Explosion("/Explosion_small.wav");
				break;
			}
			else {
				break;
			}
			
				}				
			}
			
		  }
        }
		
		
		
		
		p.tick(feat);
		p2.tick(feat2);
		
		c.tick();
		
	}
	
	private void render(Graphics g)
	{
		//BufferedImage b1;

	//	BufferStrategy bs = this.getBufferStrategy();
	//	if(bs == null)
//		{
//			createBufferStrategy(3);
//			return;
//		}
	//	 g =  bs.getDrawGraphics();
		
	//	 ((Graphics2D) g).scale(1,.5);
//////////////////////////////////////////////////
	//	 g.drawImage(image,0,0,getWidth(),getHeight(),null);
		 
		
		back.render(g);
			
		//for(int i =0;i<W.size();i++){
			
	//		W.get(i).render(g);
	//	}
		
		p.render(g);
		p2.render(g);
		c.render(g);
		health.render(g);
		health2.render(g);
		//g.drawString(score.getScore(), 300, 0);
/////////////////////////////////////////////////
		
		
		g.dispose();
		
	//	bs.show();
		
	}
	public void keyPressed(KeyEvent e){
		int key = e.getKeyCode();
		
		if(key == KeyEvent.VK_RIGHT){
			
			i--;
			if(i == 0)
				i =59;
			p.turnRight(i);
			p1right = true;
			
			
			
		}else if(key == KeyEvent.VK_LEFT){
	
			i++;
			if(i==60)
				i=1;
			p.turnLeft(i);
			p1left = true;
		
		}else if(key == KeyEvent.VK_DOWN){
			double velUp = -(Math.cos(Math.toRadians(i*6)));
			double velDown = -(Math.sin(Math.toRadians(i*6)));
			p.setVelDown(velUp,velDown);
			
		}else if(key == KeyEvent.VK_UP){
			double velUp = Math.cos(Math.toRadians(i*6));
			double velDown = Math.sin(Math.toRadians(i*6));
			p.setVelUp(velUp,velDown);
		}else if(key == KeyEvent.VK_SPACE && !is_shooting){
			is_shooting = true;
			if((!(p.tankDraw==0)) && (shoot_count1 ==0)){
			c.addEntity(new Bullet(this,p.getX(),p.getY(),i,p.getTankDraw(),feat.getShotSpeed()));
			shoot_count1 = 120/feat.getShoot();
			}
		}else if(key == KeyEvent.VK_W){
			double velUp = Math.cos(Math.toRadians(i2*6));
			double velDown = Math.sin(Math.toRadians(i2*6));
			p2.setVelUp(velUp,velDown);
		}else if(key == KeyEvent.VK_A){
			
			i2++;
			if(i2==60)
				i2=1;
			p2.turnLeft(i2);
			p2left = true;
			
		}else if(key == KeyEvent.VK_S){
			double velUp = -(Math.cos(Math.toRadians(i2*6)));
			double velDown = -(Math.sin(Math.toRadians(i2*6)));
			p2.setVelDown(velUp,velDown);
		}else if(key == KeyEvent.VK_D){
			
			i2--;
			if(i2 == 0)
				i2 =59;
			p2.turnRight(i2);
			p2right =true;
			
		}else if(key == KeyEvent.VK_ENTER && !is_shooting2){
			is_shooting2 = true;
			
			if((!(p2.tankDraw==0)) && (shoot_count2 ==0)){
			c.addEntity2(new Bullet(this,p2.getX(),p2.getY(),i2,p2.getTankDraw(),feat2.getShotSpeed()));
				shoot_count2 =(120/feat2.getShoot());
			}
		}
				
		 
	 }
	 public void keyReleased(KeyEvent e){
		 int key = e.getKeyCode();

			if(key == KeyEvent.VK_RIGHT){
				i--;
				if(i == 0)
					i =59;
				p.turnRight(i);
				p1right = false;
				
			}else if(key == KeyEvent.VK_LEFT){
				i++;
				if(i==60)
					i=1;
				p.turnLeft(i);
				p1left = true;
			}else if(key == KeyEvent.VK_DOWN){
				p.setRelDown();
				
			}else if(key == KeyEvent.VK_UP){
				p.setRelUp();
			}else if(key == KeyEvent.VK_SPACE){
				is_shooting = false;
			}else if(key == KeyEvent.VK_W){
				p2.setRelUp();
			}else if(key == KeyEvent.VK_A){
				i2++;
				if(i2==60)
					i2=1;
				p2.turnLeft(i2);
				p2right = true;
			}else if(key == KeyEvent.VK_D){
				i2--;
				if(i2 == 0)
					i2 =59;
				p2.turnRight(i2);
				p2right = false;
			}else if(key == KeyEvent.VK_S){
				p2.setRelDown();
			}else if(key == KeyEvent.VK_ENTER){
				is_shooting2 = false;
			}
	 }
	
	
	 public static void main(String args[])
	 {
		 
		 Tank tank = new Tank();
		 tank.setPreferredSize(new Dimension(WIDTH*SCALE,HEIGHT*SCALE));
		 tank.setMaximumSize(new Dimension(WIDTH*SCALE,HEIGHT*SCALE));
		 tank.setMinimumSize(new Dimension(WIDTH*SCALE,HEIGHT*SCALE));
		 
		 JFrame frame = new JFrame(tank.Title);
		
		 frame.add(tank);
		 frame.pack();
                // frame.setBackground(Color.red);
		 frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		 frame.setResizable(false);
		 frame.setLocationRelativeTo(null);
		 frame.setVisible(true);
		 tank.start();
	 }
     public BufferedImage getBack(){
             return background;
         }
	 public BufferedImage getPlayer()
	 {
			 return Player;		 
	 }
	 public BufferedImage getPlayer1(){
		 return Player1;
	 }
	 public BufferedImage getPlayer2(){
		 return Player2;
	 }
	 public BufferedImage getPlayer3(){
		 return Player3;
	 }
	 public BufferedImage getBullet1()
	 {
		 return Bullet1;
	 }
	 public BufferedImage getBullet2()
	 {
		 return Bullet2;
	 }
	 public BufferedImage getBullet3()
	 {
		 return Bullet3;
	 }
	 public int getI()
	 {
		 return i;
	 }
	 public BufferedImage getPowerUp(){
		 return power;
	 }
	 public BufferedImage smallExplosion1(){
		return small[1];
		  }
	 public BufferedImage smallExplosion2(){
			return small[2];
			  }
	 public BufferedImage smallExplosion3(){
			return small[3];
			  }
	 public BufferedImage smallExplosion4(){
			return small[4];
			  }
	 public BufferedImage smallExplosion5(){
			return small[5];
			  }
	 public BufferedImage smallExplosion6(){
			return small[6];
			  }
	 public BufferedImage largeExplosion1(){
			return large[1];
			  }
		 public BufferedImage largeExplosion2(){
				return large[2];
				  }
		 public BufferedImage largeExplosion3(){
				return large[3];
				  }
		 public BufferedImage largeExplosion4(){
				return large[4];
				  }
		 public BufferedImage largeExplosion5(){
				return large[5];
				  }
		 public BufferedImage largeExplosion6(){
				return large[6];
				  }
		 public BufferedImage largeExplosion7(){
				return large[7];
				  }
		 public void setTank1(){
			 p.setX((double) 1100);// = new Player(1100,1070,this);
			 p.setY((double) 1166);
			 p.setTankDraw(0);
			 
		 }
		 public void setTank2(){
			 p2.setX((double)170);
			 p2.setY((double) 150);
			 p2.setTankDraw(0);
			 
			 
		 }
         public void HealthReset(){
        	 health.healthReset();
        	 health2.healthReset();
         }
     public void setTank1Score(){
    	 p1Score++;
     }
     public void setTank2Score(){
    	 p2Score++;
     }
    public BufferedImage getWall(){
    	return Wall1;
    }

	@Override
	public void mouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub
	 //For shooting
		//Light
		if(e.getButton()==MouseEvent.BUTTON1){
		if((e.getX()>F_light.getX()+78) && (e.getX()<F_light.getX()+178) && (e.getY()>F_light.getY()+10) && (e.getY()<F_light.getY()+30)){
			F_light.setShoot(1);
		}
		}
		if(e.getButton()==MouseEvent.BUTTON3){
			if((e.getX()>F_light.getX()+78) && (e.getX()<F_light.getX()+178) && (e.getY()>F_light.getY()+10) && (e.getY()<F_light.getY()+30)){
				F_light.setShoot(-1);
			}
			}
		//Basic
		if(e.getButton()==MouseEvent.BUTTON1){
			if((e.getX()>F_basic.getX()+78) && (e.getX()<F_basic.getX()+178) && (e.getY()>F_basic.getY()+10) && (e.getY()<F_basic.getY()+30)){
				F_basic.setShoot(1);
			}
			}
			if(e.getButton()==MouseEvent.BUTTON3){
				if((e.getX()>F_basic.getX()+78) && (e.getX()<F_basic.getX()+178) && (e.getY()>F_basic.getY()+10) && (e.getY()<F_basic.getY()+30)){
					F_basic.setShoot(-1);
				}
				}
			//heavy
			if(e.getButton()==MouseEvent.BUTTON1){
				if((e.getX()>F_heavy.getX()+78) && (e.getX()<F_heavy.getX()+178) && (e.getY()>F_heavy.getY()+10) && (e.getY()<F_heavy.getY()+30)){
					F_heavy.setShoot(1);
				}
				}
				if(e.getButton()==MouseEvent.BUTTON3){
					if((e.getX()>F_heavy.getX()+78) && (e.getX()<F_heavy.getX()+178) && (e.getY()>F_heavy.getY()+10) && (e.getY()<F_heavy.getY()+30)){
						F_heavy.setShoot(-1);
					}
					}
		//For Damage
		//Light 
		if(e.getButton()==MouseEvent.BUTTON1){
			if((e.getX()>F_light.getX()+78) && (e.getX()<F_light.getX()+178) && (e.getY()>F_light.getY()+30) && (e.getY()<F_light.getY()+50)){
				F_light.setDamage(5);
			}
			}
			if(e.getButton()==MouseEvent.BUTTON3){
				if((e.getX()>F_light.getX()+78) && (e.getX()<F_light.getX()+178) && (e.getY()>F_light.getY()+30) && (e.getY()<F_light.getY()+50)){
					F_light.setDamage(-5);
				}
				}
			//basic
			if(e.getButton()==MouseEvent.BUTTON1){
				if((e.getX()>F_basic.getX()+78) && (e.getX()<F_basic.getX()+178) && (e.getY()>F_basic.getY()+30) && (e.getY()<F_basic.getY()+50)){
					F_basic.setDamage(5);
				}
				}
				if(e.getButton()==MouseEvent.BUTTON3){
					if((e.getX()>F_basic.getX()+78) && (e.getX()<F_basic.getX()+178) && (e.getY()>F_basic.getY()+30) && (e.getY()<F_basic.getY()+50)){
						F_basic.setDamage(-5);
					}
					}
				//heavy
				if(e.getButton()==MouseEvent.BUTTON1){
					if((e.getX()>F_heavy.getX()+78) && (e.getX()<F_heavy.getX()+178) && (e.getY()>F_heavy.getY()+30) && (e.getY()<F_heavy.getY()+50)){
						F_heavy.setDamage(5);
					}
					}
					if(e.getButton()==MouseEvent.BUTTON3){
						if((e.getX()>F_heavy.getX()+78) && (e.getX()<F_heavy.getX()+178) && (e.getY()>F_heavy.getY()+30) && (e.getY()<F_heavy.getY()+50)){
							F_heavy.setDamage(-5);
						}
						}
				
			//Shot Speed
					//light
			if(e.getButton()==MouseEvent.BUTTON1){
				if((e.getX()>F_light.getX()+78) && (e.getX()<F_light.getX()+178) && (e.getY()>F_light.getY()+50) && (e.getY()<F_light.getY()+70)){
					F_light.shotSpeed(1);
				}
				}
				if(e.getButton()==MouseEvent.BUTTON3){
					if((e.getX()>F_light.getX()+78) && (e.getX()<F_light.getX()+178) && (e.getY()>F_light.getY()+50) && (e.getY()<F_light.getY()+70)){
						F_light.shotSpeed(-1);
					}
					}
				//basic
				if(e.getButton()==MouseEvent.BUTTON1){
					if((e.getX()>F_basic.getX()+78) && (e.getX()<F_basic.getX()+178) && (e.getY()>F_basic.getY()+50) && (e.getY()<F_basic.getY()+70)){
						F_basic.shotSpeed(1);
					}
					}
					if(e.getButton()==MouseEvent.BUTTON3){
						if((e.getX()>F_basic.getX()+78) && (e.getX()<F_basic.getX()+178) && (e.getY()>F_basic.getY()+50) && (e.getY()<F_basic.getY()+70)){
							F_basic.shotSpeed(-1);
						}
						}
					//heavy
					
					if(e.getButton()==MouseEvent.BUTTON1){
						if((e.getX()>F_heavy.getX()+78) && (e.getX()<F_heavy.getX()+178) && (e.getY()>F_heavy.getY()+50) && (e.getY()<F_heavy.getY()+70)){
							F_heavy.shotSpeed(1);
						}
						}
						if(e.getButton()==MouseEvent.BUTTON3){
							if((e.getX()>F_heavy.getX()+78) && (e.getX()<F_heavy.getX()+178) && (e.getY()>F_heavy.getY()+50) && (e.getY()<F_heavy.getY()+70)){
								F_heavy.shotSpeed(-1);
							}
							}
		//Armour Saving 
						//light
				if(e.getButton()==MouseEvent.BUTTON1){
					if((e.getX()>F_light.getX()+78) && (e.getX()<F_light.getX()+178) && (e.getY()>F_light.getY()+70) && (e.getY()<F_light.getY()+90)){
						F_light.setArmour(5);
					}
					}
					if(e.getButton()==MouseEvent.BUTTON3){
						if((e.getX()>F_light.getX()+78) && (e.getX()<F_light.getX()+178) && (e.getY()>F_light.getY()+70) && (e.getY()<F_light.getY()+90)){
							F_light.setArmour(-5);
						}
						}
					//basic
					if(e.getButton()==MouseEvent.BUTTON1){
						if((e.getX()>F_basic.getX()+78) && (e.getX()<F_basic.getX()+178) && (e.getY()>F_basic.getY()+70) && (e.getY()<F_basic.getY()+90)){
							F_basic.setArmour(5);
						}
						}
						if(e.getButton()==MouseEvent.BUTTON3){
							if((e.getX()>F_basic.getX()+78) && (e.getX()<F_basic.getX()+178) && (e.getY()>F_basic.getY()+70) && (e.getY()<F_basic.getY()+90)){
								F_basic.setArmour(-5);
							}
							}
						//heavy
						if(e.getButton()==MouseEvent.BUTTON1){
							if((e.getX()>F_heavy.getX()+78) && (e.getX()<F_heavy.getX()+178) && (e.getY()>F_heavy.getY()+70) && (e.getY()<F_heavy.getY()+90)){
								F_heavy.setArmour(5);
							}
							}
							if(e.getButton()==MouseEvent.BUTTON3){
								if((e.getX()>F_heavy.getX()+78) && (e.getX()<F_heavy.getX()+178) && (e.getY()>F_heavy.getY()+70) && (e.getY()<F_heavy.getY()+90)){
									F_heavy.setArmour(-5);
								}
								}
						
		//Max Speed
							//light
					if(e.getButton()==MouseEvent.BUTTON1){
						if((e.getX()>F_light.getX()+78) && (e.getX()<F_light.getX()+178) && (e.getY()>F_light.getY()+90) && (e.getY()<F_light.getY()+110)){
							F_light.setMaxSpeed(1);
						}
						}
						if(e.getButton()==MouseEvent.BUTTON3){
							if((e.getX()>F_light.getX()+78) && (e.getX()<F_light.getX()+178) && (e.getY()>F_light.getY()+90) && (e.getY()<F_light.getY()+110)){
								F_light.setMaxSpeed(-1);
							}
							}
						//basic
						if(e.getButton()==MouseEvent.BUTTON1){
							if((e.getX()>F_basic.getX()+78) && (e.getX()<F_basic.getX()+178) && (e.getY()>F_basic.getY()+90) && (e.getY()<F_basic.getY()+110)){
								F_basic.setMaxSpeed(1);
							}
							}
							if(e.getButton()==MouseEvent.BUTTON3){
								if((e.getX()>F_basic.getX()+78) && (e.getX()<F_basic.getX()+178) && (e.getY()>F_basic.getY()+90) && (e.getY()<F_basic.getY()+110)){
									F_basic.setMaxSpeed(-1);
								}
								}
							//heavy
							if(e.getButton()==MouseEvent.BUTTON1){
								if((e.getX()>F_heavy.getX()+78) && (e.getX()<F_heavy.getX()+178) && (e.getY()>F_heavy.getY()+90) && (e.getY()<F_heavy.getY()+110)){
									F_heavy.setMaxSpeed(1);
								}
								}
								if(e.getButton()==MouseEvent.BUTTON3){
									if((e.getX()>F_heavy.getX()+78) && (e.getX()<F_heavy.getX()+178) && (e.getY()>F_heavy.getY()+90) && (e.getY()<F_heavy.getY()+110)){
										F_heavy.setMaxSpeed(-1);
									}
									}
		//	degree of Turn			
				//light
						if(e.getButton()==MouseEvent.BUTTON1){
							if((e.getX()>F_light.getX()+78) && (e.getX()<F_light.getX()+178) && (e.getY()>F_light.getY()+110) && (e.getY()<F_light.getY()+130)){
								F_light.setDegreeOfTurn(1);
							}
							}
							if(e.getButton()==MouseEvent.BUTTON3){
								if((e.getX()>F_light.getX()+78) && (e.getX()<F_light.getX()+178) && (e.getY()>F_light.getY()+110) && (e.getY()<F_light.getY()+130)){
									F_light.setDegreeOfTurn(-1);
								}
								}	
				//basic
							if(e.getButton()==MouseEvent.BUTTON1){
								if((e.getX()>F_basic.getX()+78) && (e.getX()<F_basic.getX()+178) && (e.getY()>F_basic.getY()+110) && (e.getY()<F_basic.getY()+130)){
									F_basic.setDegreeOfTurn(1);
								}
								}
								if(e.getButton()==MouseEvent.BUTTON3){
									if((e.getX()>F_basic.getX()+78) && (e.getX()<F_basic.getX()+178) && (e.getY()>F_basic.getY()+110) && (e.getY()<F_basic.getY()+130)){
										F_basic.setDegreeOfTurn(-1);
									}
									}	
				//heavy				
								if(e.getButton()==MouseEvent.BUTTON1){
									if((e.getX()>F_heavy.getX()+78) && (e.getX()<F_heavy.getX()+178) && (e.getY()>F_heavy.getY()+110) && (e.getY()<F_heavy.getY()+130)){
										F_heavy.setDegreeOfTurn(1);
									}
									}
									if(e.getButton()==MouseEvent.BUTTON3){
										if((e.getX()>F_heavy.getX()+78) && (e.getX()<F_heavy.getX()+178) && (e.getY()>F_heavy.getY()+110) && (e.getY()<F_heavy.getY()+130)){
											F_heavy.setDegreeOfTurn(-1);
										}
										}	
				
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}
	public void setFeatures2(Features f)
	{
		feat2 = f;
	}
	public void setFeatures(Features f)
	{
		feat = f;
	}
    public Features getlight()
    {
    	return F_light;
    }
    public Features getbasic()
    {
    	return F_basic;
    }
    public Features getheavy()
    {
    	return F_heavy;
    }
		 
		 
}



