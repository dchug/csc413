package com.ketan.tank;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;

public class Wall extends GameObject implements EntityA{
	private BufferedImage wall;
	
	private Boolean broke ,breakable,p;
	private int disappearance =0;
	SpriteSheet ss;String name="";
   public Wall(BufferedImage image,double x,double y,Boolean breakable,Boolean pow,String name){
	   
	   super(x,y);
	   this.wall = image;
	   this.breakable = breakable;
	   this.broke =false;
	   this.p = pow;
	   this.name = name;
	  
	  // ss = new SpriteSheet(wall);
   }
   
   public void tick()
   {
	   if(disappearance ==0)
		   broke =false;
	   if(broke)
	   disappearance--;
   }
   public void render(Graphics g)
   {
	   if(!breakable)
		   g.drawImage(wall, (int) x, (int) y, null);
	
	   if(breakable && (!broke) )
	   g.drawImage(wall, (int) x, (int) y, null);
   }
   
 public Rectangle getBound()
 {
	 
	 return new Rectangle((int) x,(int) y,wall.getWidth(),wall.getHeight());
 }
@Override
public double getX() {
	// TODO Auto-generated method stub
	return x;
}
public double getMaxX()
{
	return (x+wall.getWidth());
}

@Override
public double getY() {
	// TODO Auto-generated method stub
	return y;
}

@Override
public void setBroke(boolean broke) {
	// TODO Auto-generated method stub
	 this.broke = broke;
	 disappearance = 300;
}
public void setOnlyBroke(boolean broke) {
	// TODO Auto-generated method stub
	 this.broke = broke;
	
}
public int disappearCount()
{
	return disappearance;
}
public void setdisappearanceCount(int count){
	disappearance = count;
}
public boolean breakable(){
return breakable;	
}
public boolean checkPowerUp(){
	return p;
}

@Override
public void setX(int nextInt) {
	// TODO Auto-generated method stub
	
}

@Override
public void setY(int nextInt) {
	// TODO Auto-generated method stub
	
}
public BufferedImage getWall(){
	return wall;
}
public String getName()
{
	return name;
}

}
