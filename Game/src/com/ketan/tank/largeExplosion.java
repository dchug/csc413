package com.ketan.tank;

import java.awt.Graphics;
import java.awt.image.BufferedImage;

public class largeExplosion {
	BufferedImage[] large = new BufferedImage[7];
    Animation an;private int count;
    private  double x;
	private  double y;
	public largeExplosion(double x, double y,Tank tank){
		this.x = x;
		this.y = y;
		this.large[0] = tank.largeExplosion1();
		this.large[1] = tank.largeExplosion2();
		this.large[2] = tank.largeExplosion3();
		this.large[3] = tank.largeExplosion4();
		this.large[4] = tank.largeExplosion5();
		this.large[5] = tank.largeExplosion6();
		this.large[6] = tank.largeExplosion7();
		 
		count =50;
		
		an = new Animation(3,large[0],large[1],large[2],large[3],large[4],large[5],large[6]);
		
	}
	public void tick (){
		an.runAnimation();
	}
	public void render(Graphics g){
		an.drawAnimation(g, (int) x,(int) y, 0);
		count--;
	}
	public void setX(double x){
		this.x=x;
	}
	public void setY(double y){
		this.y = y;
	}
	public int getCount(){
		return count;
	}
}
