package com.ketan.tank;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class KeyInput extends KeyAdapter {

	Tank tank;
	
	public KeyInput(Tank tank)
	{
		this.tank = tank;
	}
	
	public void keyPressed(KeyEvent e){
		 tank.keyPressed(e);
	 }
	 public void keyReleased(KeyEvent e){
		 tank.keyReleased(e);
	 }
}
