package com.ketan.tank;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;

public interface EntityA {
	
		public void tick();
		public void render(Graphics g);
		
		public double getX();
		public double getY();
		
		public Rectangle getBound();
		public void setBroke(boolean broke);
		public int disappearCount();
		public boolean breakable();
		public boolean checkPowerUp();
		public void setX(int nextInt);
		public void setY(int nextInt);
		public void setdisappearanceCount(int i);
		public BufferedImage getWall();
		public double getMaxX();
		public String getName();
		

	


}
