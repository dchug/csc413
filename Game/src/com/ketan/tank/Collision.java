package com.ketan.tank;

import java.awt.Graphics;

public class Collision {
	smallExplosion SE;Graphics g;Tank tank;
	private int countp1,countp2;Sound sound = new Sound();
	public Collision(Tank tank){
		this.tank = tank;
		
		countp1 =1;
		countp2 =1;
	}
	public void b1p2(Player p2,Controller c,Sound sound,Healthbar health,Features f)
	{
		if(p2.getTankDraw() ==0){
			countp2=2;
		}else{
		countp2 = f.getDamage()/5;
		}
		for(int i =0;i<c.e.size();i++){
			
			if(p2.getBound().intersects(c.e.get(i).getBound())){
				sound.Explosion("/Explosion_small.wav");
				c.addExplosion(new smallExplosion(c.e.get(i).getX(),c.e.get(i).getY(),tank));
			c.removeEntity(c.e.get(i));
			health.setHealth(countp2);
			//countp2--;
			if(p2.getblast() ==0){
				
				c.addExplosion(new largeExplosion(p2.getX(),p2.getY(),tank));
				sound.Explosion("/Explosion_large.wav");
				health.healthReset();
				tank.setTank2Score();
				tank.setTank2();
				p2.setBlast(1);
			
			
			}
			}
		}
		}
		
	public void b2p1(Player p,Controller c,Sound sound,Healthbar health,Features f)
	{
		if(p.getTankDraw() ==0){
			countp1=2;}
		else{
		countp1 = f.getDamage()/5;
		}
		if(c.e2.size()>0){
		for(int i =0;i<c.e2.size();i++){
			
			if(p.getBound().intersects(c.e2.get(i).getBound())){
				sound.Explosion("/Explosion_small.wav");
				c.addExplosion(new smallExplosion(c.e2.get(i).getX(),c.e2.get(i).getY(),tank));		
			c.removeEntity2(c.e2.get(i));
			health.setHealth(countp1);
			//countp1--;
			if(p.getblast() ==0){
				
				c.addExplosion(new largeExplosion(p.getX(),p.getY(),tank));
				sound.Explosion("/Explosion_large.wav");
				tank.setTank1();
				tank.setTank1Score();
				health.healthReset();
				p.setBlast(1);
			}
			
		}
		}
		
	}
	}
		
	public int p1Damage(){
		return countp1;
	}
	public int p2Damage(){
		return countp2;
	}
	

}
