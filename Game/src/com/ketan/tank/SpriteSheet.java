package com.ketan.tank;

import java.awt.image.BufferedImage;

public class SpriteSheet {
	
	 private BufferedImage image;
	 
	 public SpriteSheet(BufferedImage ss)
	 {
		 this.image = ss;
	 }

	 public BufferedImage grabImage(int col,int row, int width, int height)
	 {
		 BufferedImage img = image.getSubimage((col*64)-64, (row*64)-64, width, height);
		 
		return img;
		 
	 }
	 public BufferedImage grabBullet(int col,int row, int width, int height)
	 {
		 BufferedImage img = image.getSubimage((col*24)-24, (row*24)-24, width, height);
		 
		return img;
		 
	 }
	 
	 public BufferedImage grabStart(int col,int row, int width, int height)
	 {
		 BufferedImage img = image.getSubimage((col*35)-35, (row*35)-35, width, height);
		 
		return img;
		 
	 }
	 public BufferedImage grabTray(int col,int row, int width, int height)
	 {
		 BufferedImage img = image.getSubimage((col*80)-80, (row*30)-30, width, height);
		 
		return img;
		 
	 }
	 public BufferedImage grabBigLeg(int col,int row, int width, int height)
	 {
		 BufferedImage img = image.getSubimage((col*40)-40, (row*40)-40, width, height);
		 
		return img;
		 
	 }
	 
	 public BufferedImage grabBigLeg2(int col,int row, int width, int height)
	 {
		 BufferedImage img = image.getSubimage((col*80)-80, (row*80)-80, width, height);
		 
		return img;
		 
	 }
	 
	 
}
