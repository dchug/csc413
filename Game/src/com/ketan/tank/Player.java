package com.ketan.tank;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.util.Random;

public class Player extends GameObject {
	
    
      private double velUp;
      private double velDown;
      private int i;
	int tankDraw =0;
      
      Sound sound;
      private BufferedImage Image,player,Player1,Player2,Player3;
      SpriteSheet ss,ss1,ss2,ss3;Random power = new Random();
      Tank tank;powerUp pow;private Healthbar health;private Controller c;
     private int blast=1,id;Features F_light,F_basic,F_heavy;private int speed=2;
	  Features f;
      
      public Player(double x,double y,Tank tank,int id)
      {
          super(x,y);
          this.tank = tank;
          this.id =id;
    	  player = tank.getPlayer();
    	  Player1 = tank.getPlayer1();
    	  Player2 = tank.getPlayer2();
    	  Player3 = tank.getPlayer3();
    	  ss = new SpriteSheet(player);
    	  ss1 = new SpriteSheet(Player1);
    	  ss2 = new SpriteSheet(Player2);
    	  ss3 = new SpriteSheet(Player3);
    	  i = 1;
    	  if(getTankDraw() == 0)
     		 Image = ss.grabImage(i, 1, 64, 64);
     	 if(getTankDraw() == 1)
     		 Image = ss1.grabImage(i, 1, 64, 64);
     	 if(getTankDraw() == 2)
     		 Image = ss2.grabImage(i, 1, 64, 64);
     	 if(getTankDraw() == 3)
     		 Image = ss3.grabImage(i, 1, 64, 64);	
    	 // SpriteSheet ss = new SpriteSheet(tank.getPlayer());
    	 // player = ss.grabImage(3, 1, 64, 64);
     	F_light =tank.getlight();
     	F_basic =tank.getbasic();
     	F_heavy =tank.getheavy();
     	
    	  
      }
      public void tick(Features f){
    	  
    	 
    	 
    	 x += velUp*speed;//Math.cos(Math.toRadians(i*6));
    	 y -=  velDown*speed;//Math.sin(Math.toRadians(i*6));
    	 
    	 if(getTankDraw() == 0){
    		 Image = ss.grabImage(i, 1, 64, 64);
    	 speed =2;
    	 }
    	 if(getTankDraw() == 1){
    		 Image = ss1.grabImage(i, 1, 64, 64);
    		 speed =f.getMAxSpeed()/2;
    	 }
    	 if(getTankDraw() == 2){
    		 Image = ss2.grabImage(i, 1, 64, 64);
    		 speed =f.getMAxSpeed()/2;
    	 }
    	 if(getTankDraw() == 3)
    	 {
    		 Image = ss3.grabImage(i, 1, 64, 64);
    		 speed =f.getMAxSpeed()/2;
    	 }
    	 
    	 
//    	 if(x<=0){
//    	    x = 0;
//    	    y = getY();
//    	 }
//    	 
//    	// if (x >= (1280-50))
//    	  //  x = 1280-50;
//    	 
//    	 if (y <= 0)
//    	    y = 0;
//    	 
//    	 if (y >= (960-64))
//    	    y = 960-64;
    	 
      }
      public void render(Graphics g)
      {
    	  
    	 g.drawImage(Image,(int) x,(int) y, null);
    	  
      }
      public double getX()
      {
		return x;
    	  
      }
      public double getY()
      {
		return y;
    	  
      }
      public void setX(double x){
    	  this.x = x;
    	  
      }
      public void setY(double y){
    	  this.y=y;
      }
      public void turnRight(int i)
      { 
    	this.i = i;
    	
     }
      public void turnLeft(int i)
      {
    	  this.i = i;
      }
      public int getI()
      {
    	  return i;
      }
      public void setVelUp(double velUp,double velDown)
      {
    	 this.velUp = velUp; //Math.cos(Math.toRadians(i*6));
    	 this.velDown = velDown; //Math.sin(Math.toRadians(i*6));
      }
      public void setVelDown(double velUp,double velDown)
      {
    	  this.velUp = velUp; //-(Math.cos(Math.toRadians(i*6)));
    	  this.velDown = velDown;  //-(Math.sin(Math.toRadians(i*6)));
      }
      public void setRelUp()
      {
    	 velUp =0;
    	 velDown = 0;
      }
      public void setRelDown()
      {
    	  velUp = 0;
    	  velDown = 0;
      }
      public Rectangle getBound()
      {
     	 return new Rectangle((int) x,(int) y,Image.getWidth(),Image.getHeight());
      }
      public void collision(Controller c,Sound sound,Healthbar health,Tank tank)
      {
    	  this.health=health; this.tank = tank;this.c =c; 
    	  this.sound = sound;
    	  for(int i =0;i < c.ea.size();i++){
    		  
    	  if(this.getBound().intersects(c.ea.get(i).getBound()))
    	  {
    		  if(!c.ea.get(i).checkPowerUp()){
    			  if(!(c.ea.get(i).breakable())) {
    				//  if(Tank.isPixelCollide(c.ea.get(i).getX(),c.ea.get(i).getY(),c.ea.get(i).getWall(),this.getX(),this.getY(),Image)){
    				    velUp = -(velUp);
     		    		 velDown =-(velDown);
    				//  }
    				  
    		 
    			  }
    			  else if( c.ea.get(i).breakable() && (!(c.ea.get(i).disappearCount() == 0))){
    				  
    		    		 c.ea.get(i).setdisappearanceCount(100);
    				  
    			  }else if(c.ea.get(i).breakable() && (c.ea.get(i).disappearCount() == 0)){
    				//  if(Tank.isPixelCollide(c.ea.get(i).getX(),c.ea.get(i).getY(),c.ea.get(i).getWall(),this.getX(),this.getY(),Image)){
    				  velUp = -(velUp);
    		    		 velDown =-(velDown);
    				//  }
    			  }
    		  }
    		  if(c.ea.get(i).checkPowerUp())
    		  {
    			  c.ea.get(i).setX(power.nextInt(1280));
    			  c.ea.get(i).setY(power.nextInt(1280));
    			  sound.Explosion("/turret.wav");
    			  if(!(((powerUp) c.ea.get(i)).getindex()==4)){
    			  setTankDraw(((powerUp) c.ea.get(i)).getindex());
    			  }else{
    				  health.setPowerUP(0);
    			  }
    			  }
    			  
    			//  c.removeEntityA(c.ea.get(i));
    		  }
    		  }
    		  
    	  
      }
      public int getTankDraw()
 	 {
 		 return tankDraw;
 	 }
      public  void setTankDraw(int tankDraw1){
    	  this.tankDraw = tankDraw1;
    	  if(getID() ==1){
    	  if(tankDraw == 1)
    		  tank.setFeatures(tank.getlight());
    	  if(tankDraw == 2)
    		  tank.setFeatures(tank.getbasic());
    	  if(tankDraw == 3)
    		  tank.setFeatures(tank.getheavy());
    	  }
    	  if(getID() ==2){
        	  if(tankDraw == 1)
        		  tank.setFeatures2(tank.getlight());
        	  if(tankDraw == 2)
        		  tank.setFeatures2(tank.getbasic());
        	  if(tankDraw == 3)
        		  tank.setFeatures2(tank.getheavy());
        	  }
      }
      public void setBlast(int i)
      {
    	 blast = i; 
    	  
      }
      public int getblast()
      {
    	  return blast;
      }
      public int getID()
      {
    	  return id;
      }
      public void setSpeed(int speed)
      {
    	  this.speed = speed;
      }
}
