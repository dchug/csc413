package com.ketan.tank;

import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;

public interface Entity {
	public void tick();
	public void render(Graphics g);
	
	public double getX();
	public double getY();
	public Rectangle getBound();
	public BufferedImage getBullet();
	public void setVelX(int i);
	
	

}
