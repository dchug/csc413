package com.ketan.rainbowGame;

import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;

import com.ketan.tank.GameObject;

public class Buttons extends GameObject  {
	BufferedImage button;
	
	
public Buttons(BufferedImage image,double x,double y)

{
	super(x,y);
	button = image; 
	
}

public void tick()
{
	
}
public void render(Graphics g)
{
	
	g.drawImage(button,(int) x,(int) y, null);
}
public Rectangle getbound()
{
	return new Rectangle((int)x,(int)y,button.getWidth(),button.getHeight());
}
public void setImage(BufferedImage image)
{
	button = image;
}
public void setX(double i){
	x= i;
}
public void setY(double i){
	y= i;
}
}
