package com.ketan.rainbowGame;

import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;

import com.ketan.tank.Controller;
import com.ketan.tank.GameObject;
import com.ketan.tank.SpriteSheet;

public class Tray extends GameObject {
	private BufferedImage tray;
	SpriteSheet s;private int count=1;private double velX;
	private Controller c;
	public Tray(BufferedImage image,double x,double y){
	  super(x,y);
	  tray = image;
	  s = new SpriteSheet(tray);
	}
	public void tick()
	{
		
		x+=velX;
		if(x<20)
			x=20;
		if(x>518)
			x=518;
		count++;
		if(count>23)
			count=1;
	}
	public void render(Graphics g){
		g.drawImage(s.grabTray(count, 1, 80, 30), (int)x,(int) y, null);
	}
    public double getX()
    {
		return x;
    	
    }
    public double getY()
    {
		return y;
    	
    }
    public void setvelX(double x)
    {
    	this.velX=x;
    }
    public double getvelX()
    {
    	return velX;
    }
    public void setY(double y)
    {
    	this.y=y;
    }
    public Rectangle getBound()
    {
    	return new Rectangle((int)x,(int)y,80,30);
    }
    public boolean collision(Controller ct)
    {
    	this.c=ct;
    	for(int i=0;i<c.ea.size();i++)
    	{
    		if(getBound().intersects(c.ea.get(i).getBound()))
    		{
    			return true;
    		}
    		
    	}
		return false;
    	
    }
}
