package com.ketan.rainbowGame;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.RenderingHints;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

import javax.swing.JFrame;
import javax.swing.JPanel;

import com.ketan.levels.Page0;
import com.ketan.levels.Page1;
import com.ketan.levels.Page2;
import com.ketan.levels.Page3;
import com.ketan.levels.Page4;
import com.ketan.tank.Background;
import com.ketan.tank.BufferedImageLoader;
import com.ketan.tank.Controller;
import com.ketan.tank.Sound;
import com.ketan.tank.Wall;
import com.ketan.tank.powerUp;

public class Rainbow extends JPanel implements Runnable,MouseListener {

	

	private static final long serialVersionUID = 1L;
   public static  final int WIDTH =304;
   public static  final int HEIGHT =228;//WIDTH/12*9;
   public static  final int SCALE =2;
   public final String Title = "Rainbow Game";
  
   private BufferedImage image = new BufferedImage(WIDTH*SCALE,HEIGHT*SCALE,BufferedImage.TYPE_INT_RGB);
   Graphics g ;
   private  boolean running = false;private Thread thread;
   
   
   //All integer and boolean variable here
   Point p;private int page=0;private int count=0;Boolean changePage=false,changePage2=false,changePage3=false,
		   changePage4 =false,changePage5=false;
   Random R = new Random();private int life=3;private int score=0;
   String map2; 
   //All images here
   private BufferedImage background1,start,help,load,quit,scores,backgroundLast,
   wall,star,Katch,big_leg_small,big_leg,small_tray,block1,block2,block3,block4,block5,block6,block7,
   block_split,block_solid,block_life,block_double;
 
   
   //All class instances here
   private Background back,back1;
   private Controller c;
   private rainPlayer Player;
   private Tray tray;
   private Sound sound;private Page0 page0;private Page1 page1;
   private Page2 page2;private Page3 page3;private Page4 page4;
   
   
   public void init(){
	   BufferedImageLoader loader = new BufferedImageLoader();
	   background1 = loader.loadImage("/Background1.png");
	   start = loader.loadImage("/Button_start.png");
	   help = loader.loadImage("/Button_help.png");
	   load= loader.loadImage("/Button_load.png");
	   quit = loader.loadImage("/Button_quit.png");
	   scores =loader.loadImage("/Button_scores.png");
	   backgroundLast = loader.loadImage("/Congratulation.png");
	   wall = loader.loadImage("/Wall.png");
	   star =loader.loadImage("/Pop_strip45.png");
	   Katch = loader.loadImage("/Katch_strip24.png");
	   big_leg_small = loader.loadImage("/Bigleg_small_strip24.png");
	   big_leg = loader.loadImage("/Bigleg_strip24.png");
	   small_tray = loader.loadImage("/Katch_small.png");
	   block1 = loader.loadImage("/Block1.png");
	   block2 = loader.loadImage("/Block2.png");
	   block3 = loader.loadImage("/Block3.png");
	   block4 = loader.loadImage("/Block4.png");
	   block5 = loader.loadImage("/Block5.png");
	   block6 = loader.loadImage("/Block6.png");
	   block7 = loader.loadImage("/Block7.png");
	   block_split = loader.loadImage("/Block_split.png");
	   block_solid = loader.loadImage("/Block_solid.png");
	   block_life = loader.loadImage("/Block_life.png");
	   block_double = loader.loadImage("/Block_double.png");
	   map2 = "/Resources/drawMap2";
	   
	  
	   
	   addKeyListener(new KeyInput1(this));
	   this.requestFocus();
	   addMouseListener(this);
	   
	    c = new Controller();
	    sound = new Sound();
	   back = new Background(background1,WIDTH,HEIGHT,SCALE);
	   back1 = new Background(backgroundLast,WIDTH,HEIGHT,SCALE);
	   life();
	  Player = new rainPlayer(star,300,300,sound,1);
	   c.addRainPlayer(new rainPlayer(star,300,300,sound,1));
	   tray = new Tray(Katch,300,440);
	   page0 = new Page0(back,c);
	   page1 = new Page1(back,c,Player,tray,this,wall,big_leg);
	   page2 = new Page2(back,Player,tray,this,big_leg_small,wall,big_leg,block1,
			 block2, block3, block_split, block_solid);
	   page3 = new Page3(sound,back,Player,tray,this,star,big_leg_small
				,wall,big_leg,block_split,block_life, block_double,block_solid,block7);
	   page4 = new  Page4(back,Player,tray,this,wall,big_leg
				,block1,block3,block2,block7);
	   
	  // life();
	   button();
	    // Drawmap("/drawMap1.csv");
	   sound.background("/rainMusic.wav");	   
   }
  
public void life()
   {
	   for(int i=0;i<life;i++){
		   c.addLife(new life(small_tray,22+(22*i),450));
		   }
   }
   
   public void button()
   {
	   c.addButton(new Buttons(start,10,430));
	   c.addButton(new Buttons(help,120,430));
	   c.addButton(new Buttons(load,230,430));
	   c.addButton(new Buttons(quit,340,430));
	   c.addButton(new Buttons(scores,450,430));
   }
   public  int randInt(int min, int max) {

	    // NOTE: Usually this should be a field rather than a method
	    // variable so that it is not re-seeded every call.
	    Random rand = new Random();

	    // nextInt is normally exclusive of the top value,
	    // so add 1 to make it inclusive
	    int randomNum = rand.nextInt((max - min) + 1) + min;

	    return randomNum;
	}
  
   private synchronized void start(){
	   if(running)
		   return;
	   
	   running = true;
	   thread = new Thread(this);
	   thread.start(); 
	   
   }
   private synchronized void stop()
   {
	   if(!running)
		   return;
	   
	   running = false;
	  
		   try {
			thread.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		   System.exit(1);
	   
		   
	   
   }

   @Override
	public void run() {
           
			
			
               
               init();
           
		
			
		
		long LastTime = System.nanoTime();
		final double amountofTicks = 60.0;
		double ns = 1000000000/amountofTicks;
		double delta = 0;
		int updates = 0;
		int frames = 0;long timer = System.currentTimeMillis();
		
		while(running)
		{
			//this is the game loop
			
                        
			long now = System.nanoTime(); 
			delta += (now - LastTime )/ns;			
			LastTime = now; 
			if(delta >= 1)
			{
				tick();
				updates++;
				delta--;
			}
			
			repaint();
			//render(g);
			

			frames++;
			
			if(System.currentTimeMillis() - timer > 1000){
				timer += 1000;
				//System.out.println(updates + "Ticks ,Fps "+ frames);
				updates = 0;
				frames = 0;
			}
			
		}
		stop();
	}
   
   public Graphics2D createGraphics2D(int w, int h) {
		Graphics2D g2 = null;
		if (image == null || image.getWidth() != w || image.getHeight() != h) {
			//System.out.println("inside if condition");
			image = (BufferedImage) createImage(w, h);
			
		}
		
		//g2 = image.createGraphics();
		
		g2 = image.createGraphics();
		g2.setBackground(getBackground());
		g2.setRenderingHint(RenderingHints.KEY_RENDERING,
				RenderingHints.VALUE_RENDER_QUALITY);
		g2.clearRect(0, 0, w, h);
		
		return g2;
	}  
   
   public void paint( Graphics g){
		 
		
		
		Dimension windowSize = getPreferredSize();	
		//System.out.println(windowSize.width);
     Graphics2D g2 = createGraphics2D(windowSize.width, windowSize.height);
    
     g2.setBackground(Color.RED);
    
     render(g);
     if(page!=0 && page!=5){
     Graphics2D gr1 = (Graphics2D)g;
     gr1.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
             RenderingHints.VALUE_ANTIALIAS_ON);
         Font font1 = new Font("Serif", Font.PLAIN, 20);
         gr1.setFont(font1);gr1.setColor(Color.RED);
         gr1.drawString("Score:"+Integer.toString(score), (int) 520,(int) 450); 
     }
     
   }
   
   
	
	
	private void tick() {
	// TODO Auto-generated method stub
		//System.out.println("size of player list  " + c.life.size());
		//This part initializes the game again in case all lives are gone
		if(c.life.size()==0)
		{
			init();			
		  page=0;life=3;
		  score=0;
		  		  
			button();
			life();
			 
		}
		
		// for the first level
		if(changePage){
			sound.Explosion("/Sound_click.wav");
			for(int i=0;i<c.b.size();i++){              //removes all the buttons
		 		c.removeButton(c.b.get(i));
		 		}
			//changePage=false;
		}
		if(count>0)
			count--;
		
		if(changePage && (count==0)){
			page++;                               //we change page to page1
		    changePage=false;	
		}
		
		//tick for first page
		if(page==1)
		{
			page1.tick(c);
			}
		
		//sets boolean to increase page number
		if(page1.page1Close()){                   //checks if page1 is closed 
			
			
			if(page==1){
				life = c.life.size();
				if(life>3)
					life=3;
			changePage2 =true;
			}
		}
		if(changePage2){
			
			changePage2=false;
				page++;
				
			}
		//for second level page
		if(page==2){
			page2.tick();			
		}
		
		if(page2.page2Close())
		{
			if(page==2){
				life = c.life.size();
				if(life>3)
					life=3;
				changePage3=true;
				count=5;
			}
		}
		
		if(changePage3)
		{
		changePage3=false;
			page++;
		}
		
		//tick for second page
		
		//for level3 page
		
		if(page==3)
		{
			page3.tick();
		}
		
		if(page3.page3Close())
		{
			if(page==3){
				life = c.life.size();
				
				changePage4=true;
				count=5;
			}
		}
		c.tick();
		if(changePage4)
		{
			changePage4=false;
			page++;
		}
		//for level 4 page
		if(page==4)
		{
			System.out.println("inside tick when page=4");
			page4.tick();
		}
		if(page4.page4Close())
		{
			if(page==4){
				life = c.life.size();
				
				changePage5=true;
				count=5;
			}
		}
		
		//congrates page
		if(changePage5){
			System.out.println("inside changePage3 "+c.ea.size());
			for(int i=0;i<c.ea.size();i++)
			{
				c.removeEntityA(c.ea.get(i));
			}
			
			System.out.println("inside changePage3 "+c.ea.size());
			
			if((c.ea.size()==0) && changePage5){
			page++;
			changePage5=false;
			}
		}
		//Checking collision
				for(int i=0;i<c.e.size();i++)
				{
					for(int j=0;j<c.ea.size();j++)
					{
						if(c.e.get(i).getBound().intersects(c.ea.get(j).getBound()))
						{
							if(page==2)
							{
								
								c.e.get(i).setVelX(-1);
							}
						}
					}
				}
				
		
	
}
	private void render(Graphics g) {
		// TODO Auto-generated method stub
		
		if(page==0){
			page0.render(g);
		}
		
		if(page==1){
			page1.render(g);
			}
		if(page==2){
			page2.render(g);
			c.render(g);

		}
		if(page==3)
		{
			page3.render(g);
			c.render(g);
		}
		if(page==4)
		{
			page4.render(g);
			c.render(g);
		}
		if(page==5){
			back1.render(g);
		}
		
	}
	 public void keyPressed(KeyEvent e){
		 int key = e.getKeyCode();
		 if(key == KeyEvent.VK_RIGHT)
		 {
			// if(page==1)
			
				 tray.setvelX(5);
			 
			 
		 }
		 if(key == KeyEvent.VK_LEFT)
		 {
			 
			
				 tray.setvelX(-5);
			 
			 
		 }
		
	 }
	 public void keyReleased(KeyEvent e){
		 int key = e.getKeyCode();
		 if(key == KeyEvent.VK_RIGHT)
		 {
			 
			 
				 tray.setvelX(0);
			 
		 }
		 if(key == KeyEvent.VK_LEFT)
		 {
			 
			 
				 tray.setvelX(0);
			 
		 }

	 }
	
	  
	 @Override
	 public void mouseClicked(MouseEvent e) {
	 	// TODO Auto-generated method stub
		 
	 	if(e.getX()>10 && e.getX()<110 && e.getY()>430 && e.getY()<462){
	 	count =5;changePage=true;
	 	}
	 }
	 @Override
	 public void mouseEntered(MouseEvent e) {
	 	// TODO Auto-generated method stub
	 	
	 }
	 @Override
	 public void mouseExited(MouseEvent e) {
	 	// TODO Auto-generated method stub
	 	
	 }
	 @Override
	 public void mousePressed(MouseEvent e) {
	 	// TODO Auto-generated method stub
	 	
	 }
	 @Override
	 public void mouseReleased(MouseEvent e) {
	 	// TODO Auto-generated method stub
	 	
	 }

	 public Point getClick()
	 {
		 
		return p;
	 }
	 public int getPage()
	 {
		 return page;
	 }
	 public void setLife() {
		 life--;
			c.removeLife(c.life.get(life));
			
		}
	 public void IncreaseLife() {
		 life++;
			c.addLife(new life(small_tray,22+(22*life),450));
			
		}
	 public void split(double x,double y,Controller c)
	 {
		 c.addRainPlayer(new rainPlayer(star,x,y,sound,2));
	 }
	 public BufferedImage getDouble()
	 {
		 return block1; 
	 }
	 public void setScore()
	 {
		 score++;
	 }
	public static void main(String args[])
	 {
		 
		 Rainbow rain = new Rainbow();
		 rain.setPreferredSize(new Dimension(WIDTH*SCALE,HEIGHT*SCALE));
		 rain.setMaximumSize(new Dimension(WIDTH*SCALE,HEIGHT*SCALE));
		 rain.setMinimumSize(new Dimension(WIDTH*SCALE,HEIGHT*SCALE));
		 
		 JFrame frame = new JFrame(rain.Title);
		
		 frame.add(rain);
		 frame.pack();
         frame.setBackground(Color.red);
		 frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		 frame.setResizable(false);
		 frame.setLocationRelativeTo(null);
		 frame.setVisible(true);
		 rain.start();
	 }
	
	
}
