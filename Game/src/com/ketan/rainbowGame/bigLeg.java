package com.ketan.rainbowGame;

import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;

import com.ketan.tank.Entity;
import com.ketan.tank.EntityA;
import com.ketan.tank.GameObject;
import com.ketan.tank.SpriteSheet;

public class bigLeg extends GameObject implements Entity {
	
	BufferedImage big,big_image,big_image2;SpriteSheet sh;
	private int count,count2;private int id;
    private boolean takeright=false,takeleft=false;
    private int velX;
	public bigLeg(BufferedImage image,double x,double y,int i,int speed)
	{
		super(x,y);
		big = image;
		sh = new SpriteSheet(big);
		id = i;
		this.velX = speed;
		
	}
	public void tick()
	{
		
		if(id==1){
			x+=velX;
		count++;
		if(count>24)
			count=1;
		big_image = sh.grabBigLeg(count, 1, 40, 40);
		}
		
		if(id==2){
			count2++;
			if(count2>24)
				count2=1;
			
		big_image2 = sh.grabBigLeg2(count2, 1, 80, 80);	
		
		if(y == 100){
			if(takeright)
				x+=5;
			if(takeleft)
				x-=5;
			
           if(x>588){
			takeleft=true;
			takeright=false;
           }
           if(x<20){
        	   takeright=true;
        	   takeleft=false;
           }
		}
		}
		
	}
	public void render(Graphics g)
	{    
		if(id==1){
	 	g.drawImage(big_image, (int) x,(int) y, null);
		}
		if(id==2){
	 	g.drawImage(big_image2, (int) x,(int) y, null);
		}
	}
	@Override
	public double getX() {
		// TODO Auto-generated method stub
		return 0;
	}
	@Override
	public double getY() {
		// TODO Auto-generated method stub
		return 0;
	}
	@Override
	public Rectangle getBound() {
		// TODO Auto-generated method stub
		if(id==1){
		return new Rectangle((int)x,(int)y,big_image.getWidth(),big_image.getHeight());
		}else
		
		if(id==2){
		return new Rectangle((int)x,(int)y,big_image2.getWidth(),big_image2.getHeight());
		}else{
			return null;
		}
		
		
	}
	

	@Override
	public BufferedImage getBullet() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public void setVelX(int i) {
		// TODO Auto-generated method stub
		 velX = velX*i;
	}
}
