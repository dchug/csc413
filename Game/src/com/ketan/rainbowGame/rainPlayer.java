package com.ketan.rainbowGame;

import java.awt.Graphics;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.util.Random;

import com.ketan.levels.Page3;
import com.ketan.tank.Controller;
import com.ketan.tank.GameObject;
import com.ketan.tank.Sound;
import com.ketan.tank.SpriteSheet;
import com.ketan.tank.Wall;

public class rainPlayer extends GameObject{
     private BufferedImage start;
     SpriteSheet s;private int count;
     private double velX,velY;double x1,y1;
     private Controller c;private Tray tray;private Sound sound;private Page3 page3;
     Random R = new Random();private Rainbow rain;String name;
     private double speed,velocity=1;
	 public rainPlayer(BufferedImage image,double  x,double y,Sound sound,double velocity)
	 {
		 super(x,y);
		 start =image;
		 s = new SpriteSheet(start);
		 count=1;
		 velX = -1;
		 velY = -1;
		 speed =.1;
		 this.sound = sound;
		 this.velocity = velocity;
	 }
	 public void tick(Controller ct)
	 {
		 this.c=ct;
		 
		
		 x+=velX*2*velocity;
		 y+=velY*2*velocity;
		 
		 if(y>556 && (c.player.size()>1)){
			 c.removeRainPlayer(this);
			 sound.Explosion("/Sound_lost.wav");
		 }else if(y>556 && (c.player.size()==1))
		 {
			 sound.Explosion("/Sound_lost.wav");
			velX=-1;
			velY=-1;
			x= R.nextInt(500);
			y=450;
			rain.setLife();
			 //x= R.nextInt(600);
		 }			 
		 count++;
		 
		 if((count)>45)
			 count=1;
		 }
	 public void render(Graphics g)
	 {
		 
		 g.drawImage(s.grabStart(count, 1, 35, 35), (int) x,(int) y, null);
		 
	 }
	 public Rectangle getBound()
	 {
		 return new Rectangle((int)x,(int)y,35,35);
	 }
	 public void setXdir(double i)
	 {
		 velX = i;
		
	 }
	 public void setYdir(double j)
	 {
		 velY=j;
		 speed +=.1;
		 if(speed>.98)
			 speed =.98;
		 
		 speed*=j;
		 velY+=speed;
	 }
	 public void setSpeed(double sp)
	 {
		 this.speed=sp;
	 }
	 public void velocity(double vel)
	 {
		 this.velocity=vel;
	 }
	 public double getYdir()
	 {
		 return velY;
	 }
	 public double getXdir()
	 {
		 return velX;
	 }
	 public void collision(Controller c2,Tray tray2,Rainbow rain2,Page3 page3)
	 {
		 this.c=c2;this.tray = tray2;this.rain=rain2;this.page3=page3;
		 if(getBound().intersects(tray.getBound()))
		 { 
			 sound.Explosion("/Sound_katch.wav");
			 int paddleLPos = (int)tray.getBound().getMinX();
         int ballLPos = (int)getBound().getMinX();

         int first = paddleLPos + 4;
         int second = paddleLPos + 16;
         int third = paddleLPos + 60;
         int fourth = paddleLPos + 80;

         if (ballLPos < first) {
             setXdir(-1);
             setYdir(-1);
         }

         if (ballLPos >= first && ballLPos < second) {
            // setXdir(-1);
             setYdir(-1 * getYdir());
         }

         if (ballLPos >= second && ballLPos < third) {
             //setXdir(0);
             setYdir(-1);
         }

         if (ballLPos >= third && ballLPos < fourth) {
             setXdir(1);
             setYdir(-1);
         }

         if (ballLPos > fourth) {
             setXdir(-1*getXdir());
             setYdir(-1);
         }
			 
		 }
		 for(int i=0;i<c.e.size();i++)
		 {
			 if(getBound().intersects(c.e.get(i).getBound()))
			 {
				 sound.Explosion("/Sound_bigleg.wav");
				 c.removeEntity(c.e.get(i));
				 rain.setScore();
				 rain.setScore();
			 }
		 }
		 for (int i=0;i<c.ea.size();i++)
		 {
			 
			 //System.out.println("bileg's bounds are"+ c.ea.get(i).getBound());
			 if(getBound().intersects(c.ea.get(i).getBound()))
					 {
				 if(!(c.ea.get(i).breakable())){
                      
	                int ballLeft = (int) getBound().getMinX();
	                int ballHeight = (int) getBound().getHeight();
	                int ballWidth = (int) getBound().getWidth();
	                int ballTop = (int) getBound().getMinY();
	                
	                Point pointRight = new Point(ballLeft + ballWidth + 1, ballTop);
	                Point pointLeft = new Point(ballLeft - 1, ballTop);
	                Point pointTop = new Point(ballLeft, ballTop - 1);
	                Point pointBottom = new Point(ballLeft, ballTop + ballHeight + 1); 
	                if(x<20)
	                {
	                	sound.Explosion("/Sound_wall.wav");
	                	setXdir(1);
	                }else if(y<20)
	                {
	                	sound.Explosion("/Sound_wall.wav");
	                	setYdir(1);
	                } else if(x>588)
	                {
	                	sound.Explosion("/Sound_wall.wav");
	                	setXdir(-1);
	                }else{
	                if (c.ea.get(i).getBound().contains(pointRight)) {
	                	//System.out.println("intersection with top right most x");
                        setXdir(-1);
                        sound.Explosion("/Sound_wall.wav");
                        
                    }
	                 if (c.ea.get(i).getBound().contains(pointLeft)) {
	                	//System.out.println("intersection with top left most x");
                        setXdir(1);
                        sound.Explosion("/Sound_wall.wav");
                    }

                    if (c.ea.get(i).getBound().contains(pointTop)) {
                    	//System.out.println("intersection with top-1 left most x");
                        setYdir(1);
                        sound.Explosion("/Sound_wall.wav");
                       
                    }

                     if (c.ea.get(i).getBound().contains(pointBottom)) {
                    	//System.out.println("intersection with bottom left most x");
                        setYdir(-1);
                        sound.Explosion("/Sound_wall.wav");
                    }

	                
					 }
					 }
				
					 
			 else
				 if((c.ea.get(i).breakable())){
					
					 sound.Explosion("/Sound_block.wav");
					 if(c.ea.get(i).getName()=="life")
					 {
						 rain.IncreaseLife();
					 }
					 if(c.ea.get(i).getName()=="block_split")
					 {
						 rain.split(c.ea.get(i).getX(),c.ea.get(i).getY(),c);
					 }if(c.ea.get(i).getName()=="double")
					 {
						 name = "double";
						 x1 = c.ea.get(i).getX();
						 y1 = c.ea.get(i).getY();
						 
					 }
		                int ballLeft = (int) getBound().getMinX();
		                int ballHeight = (int) getBound().getHeight();
		                int ballWidth = (int) getBound().getWidth();
		                int ballTop = (int) getBound().getMinY();
		                
		                Point pointRight = new Point(ballLeft + ballWidth + 1, ballTop);
		                Point pointLeft = new Point(ballLeft - 1, ballTop);
		                Point pointTop = new Point(ballLeft, ballTop - 1);
		                Point pointBottom = new Point(ballLeft, ballTop + ballHeight + 1);
		                Point leftMiddle = new Point(ballLeft, (ballTop + ballHeight/2));
		                Point rightMiddle = new Point(ballLeft + ballWidth + 1, (ballTop + ballHeight/2));
		                Point topMiddle = new Point(ballLeft + ballWidth/2, ballTop);
		                if(x<20)
		                {
		                	rain.setScore();
		                	setXdir(1);
		                }else if(y<20)
		                {
		                	setYdir(1);
		                	rain.setScore();
		                } else if(x>588)
		                {
		                	setXdir(-1);
		                	rain.setScore();
		                }else{
		                if (c.ea.get(i).getBound().contains(pointRight)) {
		                	//System.out.println("intersection with top right most x");
	                        setXdir(-1);
	                        setYdir(1);
	                        c.removeEntityA(c.ea.get(i));
	                        rain.setScore();
	                        System.out.println("c.ea.size()"+c.ea.size());
	               		 if(name=="double"){
	               			page3.setWall(x,y1);
	               			 name=null;
	               		 }
	                        
	                    }
		                 if (c.ea.get(i).getBound().contains(pointLeft)) {
		                //	System.out.println("intersection with top left most x");
	                        setXdir(1);
	                        setYdir(-1*getYdir());
	                        c.removeEntityA(c.ea.get(i));
	                        rain.setScore();
	                        System.out.println("c.ea.size()"+c.ea.size());
	               		 if(name=="double"){
	               			page3.setWall(x,y1);
	               			 name=null;
	               		 }
	                    }

	                    if (c.ea.get(i).getBound().contains(pointTop)) {
	                    	//System.out.println("intersection with top-1 left most x");
	                        setYdir(1);
	                        c.removeEntityA(c.ea.get(i));
	                        rain.setScore();
	                        System.out.println("c.ea.size()"+c.ea.size());
	               		 if(name=="double"){
	               			page3.setWall(x,y1);
	               			 name=null;
	               		 }
	                       
	                    }

	                     if (c.ea.get(i).getBound().contains(pointBottom)) {
	                    //	System.out.println("intersection with bottom left most x");
	                        setYdir(-1);
	                        setXdir(-1*getXdir());
	                        c.removeEntityA(c.ea.get(i));
	                        rain.setScore();
	                        System.out.println("c.ea.size()"+c.ea.size());
	               		 if(name=="double"){
	               			page3.setWall(x,y1);
	               			 name=null;
	               		 }
	                    }
	                     
                        }
						 }
				
					 //name=null;
				 
					 
					 }
			  
			 }
		
	 
		
		 
		 
	 }
}

