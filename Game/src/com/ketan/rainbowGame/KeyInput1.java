package com.ketan.rainbowGame;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class KeyInput1 extends KeyAdapter {

	Rainbow rain;
	
	public KeyInput1(Rainbow rain)
	{
		this.rain = rain;
	}
	 public void keyPressed(KeyEvent e){
		 rain.keyPressed(e);
	 }
	 public void keyReleased(KeyEvent e){
		 rain.keyReleased(e);
	 }
}
