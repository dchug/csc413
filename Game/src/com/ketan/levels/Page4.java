package com.ketan.levels;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Scanner;

import com.ketan.rainbowGame.Rainbow;
import com.ketan.rainbowGame.Tray;
import com.ketan.rainbowGame.bigLeg;
import com.ketan.rainbowGame.rainPlayer;
import com.ketan.tank.Background;
import com.ketan.tank.Controller;
import com.ketan.tank.Sound;
import com.ketan.tank.Wall;

public class Page4 {
	
private BufferedImage wall,wall_purple,wall_red,wall_yellow,wall_grey,big_leg;
	
	
	private Controller c;private rainPlayer Player;private Tray tray;private Rainbow rain;
	private Background back;private Sound sound;
	public Page4(Background back,rainPlayer player,Tray tray,Rainbow rain,BufferedImage wall,BufferedImage big_leg
			,BufferedImage wall_purple,BufferedImage wall_red,BufferedImage wall_yellow,BufferedImage wall_grey)
	{
		this.back= back;
		c = new Controller();
		this.Player = player;
		c.addRainPlayer(Player);
		this.tray = tray;
		this.rain = rain;
		this.wall = wall;
		this.wall_purple=wall_purple;
		this.wall_red=wall_red;
		this.wall_yellow=wall_yellow;
		this.wall_grey = wall_grey;
		this.big_leg = big_leg;
		
		
		Drawmap();
		
	}
	public void tick()
	{
		
		c.tick();
		for(int i=0;i<c.player.size();i++)
		{
			c.player.get(i).collision(c,tray,rain,null);
		}
		
		tray.tick();
		tray.collision(c);
		
	}
	public void render(Graphics g)
	{
		back.render(g);
		tray.render(g);
		c.render(g);
	}
	public boolean page4Close(){
		
		if(c.e.size()==0)
		{
			for(int i=0;i<c.ea.size();i++)
			{
				c.removeEntityA(c.ea.get(i));
			}
			for(int i=0;i<c.player.size();i++)
			{
				c.removeRainPlayer(c.player.get(i));
			}
		
		if(c.ea.size()==0 && c.player.size()==0){
		return true;
		}
		else{
			return false;
		}
		}else{
		return false;
		}
	}
	
	
	public void Drawmap()   {
		
		   
		Scanner mapFile = null;
	
                InputStream in ;
                in = getClass().getResourceAsStream("/drawMap4.csv"); 
		
		mapFile = new Scanner(new BufferedReader(new InputStreamReader(in)));
    
           
		 mapFile.useDelimiter(",|\\n|\\r");
	     String nextLine = null;
            
		   for(int i=0;i<23;i++)
		    {
			 for(int j=0;j<32;j++){
             String next = mapFile.next();
             System.out.println("next "+next);
            if (next.equals("")) {
               //j--;
               continue;
           }
           int n = Integer.parseInt(next);
           if(n == 1)
            {
        	c.addEntityA(new Wall(wall,j*20,20*i,false,false,null));
             }
           if(n == 2)
           {
        	  // System.out.println("big leg initialized");
        	   c.addEntity(new bigLeg(big_leg,20*j,20*i,2,0));
       	 
            }
           if(n==3)
           {
        	   c.addEntityA(new Wall(wall_purple,j*20,20*i,true,false,null));
           }
          
          /* if(n==4)
           {
        	   c.addEntityA(new Wall(wall_red,j*20,20*i,false,false,null));
           }if(n==5)
           {
        	   c.addEntityA(new Wall(wall_yellow,j*20,20*i,false,false,null));
           }if(n==6)
           {
        	   c.addEntityA(new Wall(wall_grey,j*20,20*i,false,false,null));
           }
           */
              } 
		  }
		   mapFile.close();
		   
	   }

}
