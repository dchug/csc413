package com.ketan.levels;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Random;
import java.util.Scanner;

import com.ketan.rainbowGame.Rainbow;
import com.ketan.rainbowGame.Tray;
import com.ketan.rainbowGame.bigLeg;
import com.ketan.rainbowGame.rainPlayer;
import com.ketan.tank.Background;
import com.ketan.tank.Controller;
import com.ketan.tank.Sound;
import com.ketan.tank.Wall;

public class Page3 {
	
	
	private BufferedImage star,big_leg_small,wall,big_leg,block_split,
	block_solid,block_life,block_double,block7;
	
	private Controller c;private rainPlayer Player;private Tray tray;private Rainbow rain;
	private Background back;private Sound sound;
	public Page3(Sound sound,Background back,rainPlayer player,Tray tray,Rainbow rain,BufferedImage star,BufferedImage big_leg_small
			,BufferedImage wall,BufferedImage big_leg,BufferedImage block_split,BufferedImage block_life,BufferedImage block_double,BufferedImage block_solid,BufferedImage block7)
	{
		this.sound = sound;
		this.star=star;
		this.back= back;
		c = new Controller();
		this.Player=player;
		c.addRainPlayer(player);
		this.tray = tray;
		this.rain = rain;
		this.big_leg_small = big_leg_small;
		this.wall = wall;
		this.big_leg = big_leg;
		this.block_split= block_split;
		this.block_life=block_life;
		this.block_double =block_double;
		this.block_solid = block_solid;
		this.block7= block7;
		Drawmap();
		
	}
	public void tick()
	{
		//System.out.println("ca list size() "+c.ea.size());
		
		c.tick();
		//Player.tick();
		for(int i=0;i<c.player.size();i++)
		{
			c.player.get(i).collision(c,tray,rain,this);
		}
		tray.tick();
		//tray.collision(c);
		
	}
	public void render(Graphics g)
	{
		//System.out.println("c.player.size() "+ c.player.size());
		
		back.render(g);
	   tray.render(g);
	   c.render(g);
	  
	}
	public boolean page3Close(){
		
		if(c.e.size()==0)
		{
			for(int i=0;i<c.ea.size();i++)
			{
				c.removeEntityA(c.ea.get(i));
			}
		
		if(c.ea.size()==0){
		return true;
		}
		else{
			return false;
		}
		}else{
		return false;
		}
	}
	
	
	public void setWall(double x1,double y1)
	{
		 c.addEntityA(new Wall(rain.getDouble(),x1,y1,true,false,null));
	}
	
	public void Drawmap()   {
			
		   
		Scanner mapFile = null;
	
                InputStream in ;
                in = getClass().getResourceAsStream("/drawMap2.csv"); 
		
		mapFile = new Scanner(new BufferedReader(new InputStreamReader(in)));
    
           
		 mapFile.useDelimiter(",|\\n|\\r");
	     String nextLine = null;
            
		   for(int i=0;i<23;i++)
		    {
			 for(int j=0;j<32;j++){
             String next = mapFile.next();
            if (next.equals("")) {
               //j--;
               continue;
           }
           int n = Integer.parseInt(next);
           if(n == 1)
            {
        	c.addEntityA(new Wall(wall,j*20,20*i,false,false,null));
             }
           if(n == 2)
           {
        	   c.addEntity(new bigLeg(big_leg_small,20*j,20*i,1,0));
       	 
            }
           if(n == 3)
           {
        	   c.addEntity(new bigLeg(big_leg,20*j,20*i,2,0));
       	 
            }
           if(n==11)
           {
        	   c.addEntityA(new Wall(block_solid,j*20,20*i,false,false,null));
           }
           if(n==7)
           {
        	   c.addEntityA(new Wall(block7,j*20,20*i,true,false,null));
           }
           if(n==12){
        	   c.addEntityA(new Wall(block_life,j*20,20*i,true,false,"life"));
           }
           if(n==13)
           {
        	   c.addEntityA(new Wall(block_double,j*20,20*i,true,false,"double")); 
           }
              } 
		  }
		   mapFile.close();
		   
	   }

}
