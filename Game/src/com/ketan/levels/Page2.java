package com.ketan.levels;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Scanner;

import com.ketan.rainbowGame.Rainbow;
import com.ketan.rainbowGame.Tray;
import com.ketan.rainbowGame.bigLeg;
import com.ketan.rainbowGame.rainPlayer;
import com.ketan.tank.Background;
import com.ketan.tank.Controller;
import com.ketan.tank.Sound;
import com.ketan.tank.Wall;

public class Page2 {
	private BufferedImage big_leg_small,wall,big_leg,block1,block2,block3,block_split,
	block_solid;
	
	private Controller c;private rainPlayer Player;private Tray tray;private Rainbow rain;
	private Background back;private Sound sound;
	public Page2(Background back,rainPlayer player,Tray tray,Rainbow rain,BufferedImage big_leg_small,BufferedImage wall,BufferedImage big_leg,BufferedImage block1,
			BufferedImage block2,BufferedImage block3,BufferedImage block_split,BufferedImage block_solid)
	{
		this.back= back;
		c = new Controller();
		this.Player = player;
		c.addRainPlayer(Player);
		this.tray = tray;
		this.rain = rain;
		this.big_leg_small = big_leg_small;
		this.wall = wall;
		this.big_leg = big_leg;
		this.block1 = block1;
		this.block2 = block2;
		this.block3  = block3;
		this.block_split= block_split;
		this.block_solid = block_solid;
		for(int i=0;i<c.player.size();i++){
		c.player.get(i).velocity(2);
		}
		Drawmap();
		
	}
	public void tick()
	{
		
		c.tick();
		for(int i=0;i<c.player.size();i++)
		{
			c.player.get(i).collision(c,tray,rain,null);
		}
		
		tray.tick();
		tray.collision(c);
		
	}
	public void render(Graphics g)
	{
		back.render(g);
		tray.render(g);
		c.render(g);
	}
	public boolean page2Close(){
		
		if(c.e.size()==0)
		{
			for(int i=0;i<c.ea.size();i++)
			{
				c.removeEntityA(c.ea.get(i));
			}
			for(int i=0;i<c.player.size();i++)
			{
				c.removeRainPlayer(c.player.get(i));
			}
		}
		if((c.ea.size()==0) &&(c.player.size() == 0)){
		return true;
		}else{
			return false;
		}
	}
	public void Drawmap()   {
		 			
		   
			Scanner mapFile = null;
		
	                InputStream in ;
	                in = getClass().getResourceAsStream("/drawMap1.csv"); 
			
			mapFile = new Scanner(new BufferedReader(new InputStreamReader(in)));
	    
	           
			 mapFile.useDelimiter(",|\\n|\\r");
		     String nextLine = null;
	            
			   for(int i=0;i<23;i++)
			    {
				 for(int j=0;j<32;j++){
	             String next = mapFile.next();
	            if (next.equals("")) {
	               //j--;
	               continue;
	           }
	           int n = Integer.parseInt(next);
	           if(n == 1)
	            {
	        	c.addEntityA(new Wall(wall,j*20,20*i,false,false,null));
	             }
	           if(n == 2)
	           {
	        	   c.addEntity(new bigLeg(big_leg_small,20*j,20*i,1,0));
	       	  // System.out.println(j*32+"for j="+j+"for i="+i+" "+i*32);
	       	   //  c.addEntityA(new Wall(Wall2,j*32,32*i,true,false));
	            }
	           if(n == 3)
	           {
	        	   c.addEntity(new bigLeg(big_leg,20*j,20*i,2,0));
	       	  // System.out.println(j*32+"for j="+j+"for i="+i+" "+i*32);
	       	   //  c.addEntityA(new Wall(Wall2,j*32,32*i,true,false));
	            }
	           if(n==4)
	           {
	        	   c.addEntityA(new Wall(block1,j*20,20*i,true,false,"block1"));
	           }
	           if(n==5)
	           {
	        	   c.addEntityA(new Wall(block2,j*20,20*i,true,false,"block2"));
	           }
	           if(n==6)
	           {
	        	   c.addEntityA(new Wall(block3,j*20,20*i,true,false,"block3"));
	           }
	           if(n==10)
	           {
	        	   c.addEntityA(new Wall(block_split,j*20,20*i,true,false,"block_split"));
	           }
	           if(n==11)
	           {
	        	   c.addEntityA(new Wall(block_solid,j*20,20*i,true,false,"block_solid"));
	           }
	           
	           
	              } 
			  }
			   mapFile.close();
			   
		   }

}
