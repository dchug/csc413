package com.ketan.levels;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Scanner;

import com.ketan.rainbowGame.Rainbow;
import com.ketan.rainbowGame.Tray;
import com.ketan.rainbowGame.bigLeg;
import com.ketan.rainbowGame.rainPlayer;
import com.ketan.tank.Background;
import com.ketan.tank.Controller;
import com.ketan.tank.Sound;
import com.ketan.tank.Wall;

public class Page1 {
	private BufferedImage wall,big_leg;
	
	
	private Controller c;private rainPlayer Player;private Tray tray;private Rainbow rain;
	private Background back;private Sound sound;
	public Page1(Background back,Controller c,rainPlayer player,Tray tray,Rainbow rain,BufferedImage wall,BufferedImage big_leg)
	{
		this.back= back;
		this.c = c;
		this.Player = player;
		this.tray = tray;
		this.rain = rain;
		
		this.wall = wall;
		this.big_leg = big_leg;
		
		
		Drawmap();
		
	}
	public void tick(Controller c)
	{
		this.c=c;
		c.tick();
		for(int i=0;i<c.player.size();i++)
		{
			c.player.get(i).collision(c,tray,rain,null);
		}
		
		tray.tick();
		tray.collision(c);
		
	}
	public void render(Graphics g)
	{
		back.render(g);
		tray.render(g);
		c.render(g);
	}
	public boolean page1Close(){
		
		if(c.e.size()==0)
		{
			for(int i=0;i<c.ea.size();i++)
			{
				c.removeEntityA(c.ea.get(i));
			}
			for(int i=0;i<c.player.size();i++)
			{
				c.removeRainPlayer(c.player.get(i));
			}

					
		}
		if(c.ea.size()==0) {
		return true;
		}else{
			return false;
		}
	}
	
	public void Drawmap()   {
			
		   
		Scanner mapFile = null;
	
                InputStream in ;
                in = getClass().getResourceAsStream("/drawMap3.csv"); 
		
		mapFile = new Scanner(new BufferedReader(new InputStreamReader(in)));
    
           
		 mapFile.useDelimiter(",|\\n|\\r");
	     String nextLine = null;
            
		   for(int i=0;i<23;i++)
		    {
			 for(int j=0;j<32;j++){
             String next = mapFile.next();
            if (next.equals("")) {
               //j--;
               continue;
           }
           int n = Integer.parseInt(next);
           if(n == 1)
            {
        	c.addEntityA(new Wall(wall,j*20,20*i,false,false,null));
             }
           
           if(n == 3)
           {
        	   c.addEntity(new bigLeg(big_leg,20*j,20*i,2,0));
       	  // System.out.println(j*32+"for j="+j+"for i="+i+" "+i*32);
       	   //  c.addEntityA(new Wall(Wall2,j*32,32*i,true,false));
            }
          
           
              } 
		  }
		   mapFile.close();
		   
	   }
}
