package com.ketan.levels;

import java.awt.Graphics;

import com.ketan.tank.Background;
import com.ketan.tank.Controller;


public class Page0 {
	
	
	//Class instances are here
	private Background back;private int page=0;
    private Controller c;
    
	public Page0(Background back,Controller c)
	{
		
		this.back=back;
		this.c= c;
		
	}
	public void tick()
	{
		
		
		
	}
	public void render(Graphics g)
	{
		
		back.render(g);
		for(int i=0;i<c.b.size();i++)
		{
		c.b.get(i).render(g);
		}
	}
	public void setPage(int i)
	{
		page = i;
	}

}
